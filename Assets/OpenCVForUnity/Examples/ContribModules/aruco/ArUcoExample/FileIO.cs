﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;


namespace FILE_IO {
	public class FileIO {

		public class Data {

			public static Data operator+ (Data x, Data y) {
				if (x == null)
					return null;
				if (y == null)
					return x;
				return new Data (x.id, x.frame, x.pos + y.pos, x.far_scale + y.far_scale);
			}

			public static Data operator- (Data x, Data y) {
				if (x == null)
					return null;
				if (y == null)
					return x;
				return new Data (x.id, x.frame, x.pos - y.pos, x.far_scale - y.far_scale);
			}

			public static Data operator* (Data x, float mul) {
				if (x == null)
					return null;
				return new Data (x.id, x.frame, x.pos * mul, x.far_scale * mul);
			}

			public static Data operator/ (Data x, float dev) {
				if (x == null)
					return null;
				if (dev == 0)
					return x;
				return new Data (x.id, x.frame, x.pos / dev, x.far_scale / dev);
			}

			public static Data Lerp(Data x, Data y, float p) {
				if (x == null)
					return null;
				if (y == null)
					return x;
				return new Data(
					x.id, x.frame, 
					Vector3.Lerp(x.pos, y.pos, p), 
					Mathf.Lerp(x.far_scale, y.far_scale, p));
			}

			public string toString() {
				return "id : " + id + " frame : " + frame + " pos : " + pos + " far_scale : " + far_scale;
			}

			public int id;
			public int frame;
			public Vector3 pos;
			public float far_scale;

			public Data(string str) {
				string[] parsed = str.Split(',');
				if(parsed == null || parsed.Length != 6) {
					return;
				}
				id = int.Parse(parsed[0]);
				frame = int.Parse(parsed[1]);
				pos = new Vector3(float.Parse(parsed[2]), float.Parse(parsed[3]), float.Parse(parsed[4]));
				far_scale = float.Parse(parsed[5]);
			}

			public Data(int id_, int frame_, Vector3 pos_, float far_scale_) {
				id = id_;
				frame = frame_;
				pos = pos_;
				far_scale = far_scale_;
			}

			public string getString() {
				string result = id + "," + frame + "," + pos.x + "," + pos.y + "," + pos.z + "," + far_scale + ";";
				return result;
			}
		}

		string file_full_path;
		FileInfo fi;
		StreamReader reader = null;
		StreamWriter writer = null;
		string body;
		List<Data> old_datas;
		List<Data> old_datas_reverse;
		List<Data> new_datas;

		public FileIO(string file_name) {
			file_full_path = Application.dataPath + "/" + file_name + ".txt";
			fi = new FileInfo (file_full_path);
			if (fi.Exists) {
				reader = new StreamReader (fi.OpenRead (), Encoding.UTF8);
				body = reader.ReadToEnd ();
				reader.Close ();
				reader = null;
				writer = new StreamWriter (fi.OpenWrite ());
			} else {
				writer = fi.CreateText ();
				body = "";
			}

			new_datas = new List<Data> ();
			old_datas = new List<Data> ();
			string[] lineds = body.Split (';');
			foreach (string line in lineds) {
				Data data = new Data (line);
				old_datas.Add (data);
			}
			old_datas_reverse = old_datas;
			old_datas_reverse.Reverse ();

			Debug.Log ("file_full_path : " + file_full_path);
		}

		public void addData(int id, int frame, Vector3 pos, float far_scale) {
			Data data = new Data (id, frame, pos, far_scale);
			new_datas.Add (data);
		}

		public void write2file() {
			body = "";
			foreach (Data data in new_datas) {
				body += data.getString ();
			}
			writer.Write (body);
		}

		public void close() {
			if (reader != null) {
				reader.Close ();
			}
			if (writer != null) {
				writer.Close ();
			}
		}

		public Data getData(int id, int now) {
			foreach (Data data in old_datas) {
				if (data.id == id && data.frame == now) {
					return data;
				}
			}
			return null;
		}

		public Data getFeatureData(int id, int now, int duration = 3) {
			foreach (Data data in old_datas) {
				if (data.id == id && data.frame >= now) {
					int diff = data.frame - now;
					if (diff < duration) {
						return data;
					}
				}
			}
			return null;
		}

		public Data getPastData(int id, int now, int duration = 3) {
			
			foreach (Data data in old_datas_reverse) {
				if (data.id == id && data.frame < now) {
					int diff = now - data.frame;
					if (diff < duration) {
						return data;
					}
				}
			}
			return null;
		}

		public Data calculatingLerp(Data from_data, Data to_data, int cur_frame) {
			int frame_diff = to_data.frame - from_data.frame;
			float parsentage = (float)(cur_frame - from_data.frame) / (float)frame_diff;
			Vector3 pos_diff = Vector3.Lerp(from_data.pos, to_data.pos, parsentage);
			float far_scale_diff = Mathf.Lerp (from_data.far_scale, to_data.far_scale, parsentage);
			Data data_ = new Data (to_data.id, frame_diff, pos_diff, far_scale_diff);
			return data_;
		}
	}


}
