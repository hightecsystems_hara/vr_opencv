﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using FILE_IO;

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif
using OpenCVForUnity;

namespace OpenCVForUnityExample
{
	public class ArUcoVideo : MonoBehaviour
	{	
		public int dictionaryId;
		public int[] targetIds;
		public GameObject[] targetObjs;
		public GameObject[] ARGameObjects_SkyBox;
		public Texture2D imgTexture;
		public bool showRejected = true;
		public bool applyEstimationPose = true;
		public float markerLength = 100;
		public Camera ARCamera;
		//public bool shouldMoveARCamera = false;

		[SerializeField] Vector2 POINT_CARIBRATION_ADD = new Vector2 (0, 0);
		[SerializeField] Vector2 POINT_CARIBRATION_MULTIPLE = new Vector2 (1, 1);
		[SerializeField] float SkyBoxScaleFactor = 4.1f;
		[SerializeField] GameObject INDICATOR = null;
		[SerializeField] float MAX_SIZE = 1.0f;
		[SerializeField] float distance_ratio = 0.2f;
		[SerializeField] GameObject skyBox;
		[SerializeField] string VIDEO_FILE_PATH = "";
		[SerializeField] GameObject USER;
		[SerializeField] bool IS_AUTO_PLAY = true;
		[SerializeField] GameObject GIMIC_NEST = null;
		[SerializeField] bool IS_SERCHING_MODE = false;
		[SerializeField] bool IS_SHOW_GIMIC_IN_SEARCHING = false;
		[SerializeField] int MODIFING_FRAME_DURATION = 5;
		[SerializeField] float SORT_ORDER_RESPECT_FAR_SCALE = 0.1f;
		[SerializeField] bool IS_USED_FRAME_START_END = false;
		[SerializeField] int START_FRAME = 0;
		[SerializeField] int END_FRAME = 100;

		[SerializeField] int ADAPTIVE_THRESH_WIN_SIZE_MIN = 3;
		[SerializeField] int ADAPTIVE_THRESH_WIN_SIZE_MAX = 23;
		[SerializeField] int ADAPTIVE_THRESH_WIN_SIZE_STEP = 10;
		[SerializeField] double MIN_MARKER_PERIMETER_RATE = 0.01;
		[SerializeField] double MAX_MARKER_PERIMETER_RATE = 4.0;
		[SerializeField] double POLYGONAL_APPROX_ACCURACY_RATE = 0.05;
		[SerializeField] double PERSPECTIVE_REMOVE_IGNORED_MARGIN_PERCELL = 0.2;
		[SerializeField] int PERSPECTIVE_REMOVE_PIXEL_PERCELL = 10;
		[SerializeField] double MIN_CORNER_DISTANCE_RATE = 0.05;
		[SerializeField] int MIN_DISTANCE_TO_BORDRE = 3;
		[SerializeField] int MARKER_BORDER_BITS = 1;
		[SerializeField] int MIN_OTSU_STD_DEV = 5;
		[SerializeField] int CORNERREFINEMENT_MAX_ITERATIONS = 30;
		[SerializeField] double CORNER_REFINEMENT_MIN_ACCURACY = 0.1;
		[SerializeField] GameObject debug_arrow = null;
		[SerializeField] float LOCOMOTION_SENSITIVITY = 0.5f;

		public int frame_count = 0;
		public int total_frame_count = 0;

		Texture2D videoTexture;
		VideoCapture videoCapture;
		Mat originalMat;
		Mat grayMat;
		float imageSizeScale = 1.0f;
		float widthScale, heightScale;
		bool isShownDebugLogs = false;
		float original_y_size;
		int frameWidth = 0;
		int frameHeight = 0;
		UnityEngine.UI.Text indicator_text = null;
		Dictionary<int, FileIO.Data> lastDatas = new Dictionary<int, FileIO.Data>();

		FileIO fileIO = null;
		bool is_finished = false;

		void Awake() {
			string filename = VIDEO_FILE_PATH.Replace ("/", "_").Replace(".", "_");
			fileIO = new FileIO (filename);
			if (INDICATOR != null) {
				indicator_text = INDICATOR.GetComponent<UnityEngine.UI.Text> ();
			}
			foreach (int id in targetIds) {
				lastDatas.Add (id, null);
			}
		}

		void loadVideo() {
			videoCapture = new VideoCapture ();
			videoCapture.open (Utils.getFilePath (VIDEO_FILE_PATH));

			originalMat = new Mat();
			if (!videoCapture.isOpened ()) {
				Debug.LogError ("video catpure open error");
			}

			total_frame_count = (int)videoCapture.get(Videoio.CAP_PROP_FRAME_COUNT);
			videoCapture.grab ();
			videoCapture.retrieve (originalMat, 0);
			frameWidth = originalMat.cols ();
			frameHeight = originalMat.rows ();
			videoTexture = new Texture2D (frameWidth, frameHeight, TextureFormat.RGB24, false);
			gameObject.transform.localScale = new Vector3 ((float)frameWidth, (float)frameHeight, 1);

			float skybox_scale = (((float)frameWidth) / (Mathf.PI)) * SkyBoxScaleFactor;
			skyBox.transform.localScale = new Vector3 (skybox_scale, -skybox_scale, -skybox_scale);
			original_y_size = (float)frameHeight;

			widthScale = (float)Screen.width / (float)frameWidth;
			heightScale = (float)Screen.height / (float)frameHeight;
			if (widthScale < heightScale) {
				imageSizeScale = (float)Screen.height / (float)Screen.width;
			} else {
			}

			videoCapture.set (Videoio.CAP_PROP_POS_FRAMES, 0);
			gameObject.GetComponent<Renderer> ().material.mainTexture = videoTexture;

			originalMat.release();
		}

		void getVideoTexture() {
			if (videoCapture.isOpened ()) {
				videoCapture.read (originalMat);
				frame_count++;
			}
		}

		void showDebug(string str) {
			if (!isShownDebugLogs) {
				Debug.Log (str);
			}
		}

		void UpdateARObject() {
			Mat rgbMat = new Mat();
			Imgproc.cvtColor (originalMat, rgbMat, Imgproc.COLOR_BGR2RGB);

			if (!IS_SERCHING_MODE) {
				videoTexture = new Texture2D (rgbMat.cols (), rgbMat.rows (), TextureFormat.RGBA32, false);
				Utils.matToTexture2D (rgbMat, videoTexture);
				ReplaceTexture (gameObject.GetComponent<Renderer> ().material, videoTexture);
				if (skyBox != null) {
					Texture2D videoTexture_without_ArucoInfo 
					= new Texture2D (originalMat.cols (), originalMat.rows (), TextureFormat.RGBA32, false);
					Imgproc.cvtColor (originalMat, originalMat, Imgproc.COLOR_BGR2RGB);
					Utils.matToTexture2D (originalMat, videoTexture_without_ArucoInfo);
					ReplaceTexture (skyBox.GetComponent<Renderer> ().material, videoTexture_without_ArucoInfo);
				}
				for (int i = 0; i < targetIds.Length; i++) {
					procMovingObjectFromFile (targetIds[i], ARGameObjects_SkyBox[i]);
				}
				rgbMat.release ();
				return;
			}

			float width = rgbMat.width ();
			float height = rgbMat.height ();
			int max_d = (int)Mathf.Max (width, height);
			double fx = max_d;
			double fy = max_d;
			double cx = width / 2.0f;
			double cy = height / 2.0f;
			Mat camMatrix = new Mat (3, 3, CvType.CV_64FC1);
			camMatrix.put (0, 0, fx);
			camMatrix.put (0, 1, 0);
			camMatrix.put (0, 2, cx);
			camMatrix.put (1, 0, 0);
			camMatrix.put (1, 1, fy);
			camMatrix.put (1, 2, cy);
			camMatrix.put (2, 0, 0);
			camMatrix.put (2, 1, 0);
			camMatrix.put (2, 2, 1.0f);
			MatOfDouble distCoeffs = new MatOfDouble (0, 0, 0, 0);

			Size imageSize = new Size (width * imageSizeScale, height * imageSizeScale);
			double apertureWidth = 0;
			double apertureHeight = 0;
			double[] fovx = new double[1];
			double[] fovy = new double[1];
			double[] focalLength = new double[1];
			Point principalPoint = new Point (0, 0);
			double[] aspectratio = new double[1];

			Calib3d.calibrationMatrixValues (
				camMatrix, 
				imageSize, 
				apertureWidth, 
				apertureHeight, 
				fovx, 
				fovy, 
				focalLength, 
				principalPoint, 
				aspectratio);

			double fovXScale 
			= (2.0 * Mathf.Atan (
				(float)(imageSize.width / (2.0 * fx)))) 
				/ (Mathf.Atan2 ((float)cx, (float)fx) + Mathf.Atan2 ((float)(imageSize.width - cx), (float)fx));
			double fovYScale 
			= (2.0 * Mathf.Atan (
				(float)(imageSize.height / (2.0 * fy)))) 
				/ (Mathf.Atan2 ((float)cy, (float)fy) + Mathf.Atan2 ((float)(imageSize.height - cy), (float)fy));

			showDebug ("camMatrix " + camMatrix.dump ());
			showDebug ("distCoeffs " + distCoeffs.dump ());
			showDebug ("imageSize " + imageSize.ToString ());
			showDebug ("apertureWidth " + apertureWidth);
			showDebug ("apertureHeight " + apertureHeight);
			showDebug ("fovx " + fovx [0]);
			showDebug ("fovy " + fovy [0]);
			showDebug ("focalLength " + focalLength [0]);
			showDebug ("principalPoint " + principalPoint.ToString ());
			showDebug ("aspectratio " + aspectratio [0]);
			showDebug ("fovXScale " + fovXScale);
			showDebug ("fovYScale " + fovYScale);

			if (widthScale < heightScale) {
				ARCamera.fieldOfView = (float)(fovx [0] * fovXScale);
			} else {
				ARCamera.fieldOfView = (float)(fovy [0] * fovYScale);
			}
				
			Dictionary dictionary = Aruco.getPredefinedDictionary (dictionaryId);

			DetectorParameters detectorParams = DetectorParameters.create ();
			detectorParams.set_adaptiveThreshWinSizeMin (ADAPTIVE_THRESH_WIN_SIZE_MIN);
			detectorParams.set_adaptiveThreshWinSizeMax (ADAPTIVE_THRESH_WIN_SIZE_MAX);
			detectorParams.set_adaptiveThreshWinSizeStep (ADAPTIVE_THRESH_WIN_SIZE_STEP);
			detectorParams.set_minMarkerPerimeterRate (MIN_MARKER_PERIMETER_RATE);
			detectorParams.set_maxMarkerPerimeterRate (MAX_MARKER_PERIMETER_RATE);
			detectorParams.set_perspectiveRemoveIgnoredMarginPerCell (PERSPECTIVE_REMOVE_IGNORED_MARGIN_PERCELL);
			detectorParams.set_perspectiveRemovePixelPerCell (PERSPECTIVE_REMOVE_PIXEL_PERCELL);
			detectorParams.set_polygonalApproxAccuracyRate (POLYGONAL_APPROX_ACCURACY_RATE);
			detectorParams.set_minCornerDistanceRate (MIN_CORNER_DISTANCE_RATE);
			detectorParams.set_minDistanceToBorder (MIN_DISTANCE_TO_BORDRE);
			detectorParams.set_markerBorderBits (MARKER_BORDER_BITS);
			detectorParams.set_minOtsuStdDev (MIN_OTSU_STD_DEV);
			detectorParams.set_cornerRefinementMaxIterations (CORNERREFINEMENT_MAX_ITERATIONS);
			detectorParams.set_cornerRefinementMinAccuracy (CORNER_REFINEMENT_MIN_ACCURACY);

			Mat ids = new Mat ();
			List<Mat> corners = new List<Mat> ();
			List<Mat> rejected = new List<Mat> ();
			Mat rvecs = new Mat ();
			Mat tvecs = new Mat ();
			Mat rotMat = new Mat (3, 3, CvType.CV_64FC1);
			List<int> detected_indicies = new List<int> ();

			Aruco.detectMarkers (rgbMat, dictionary, corners, ids, detectorParams, rejected, camMatrix, distCoeffs);
			if (applyEstimationPose && ids.total () > 0)
				Aruco.estimatePoseSingleMarkers (corners, markerLength, camMatrix, distCoeffs, rvecs, tvecs);

			if (IS_SERCHING_MODE && IS_SHOW_GIMIC_IN_SEARCHING) {
				foreach (GameObject skyObj in ARGameObjects_SkyBox) {
					skyObj.transform.position = GIMIC_NEST.transform.position;
				}
			}


			if (ids.total () > 0) {
				Aruco.drawDetectedMarkers (rgbMat, corners, ids, new Scalar (0, 255, 0));
				if (applyEstimationPose) {
					for (int i = 0; i < ids.total (); i++) {
						
						double[] temp = ids.get (i, 0);
						int id = (int)temp [0];
						//bool is_small_one = id >= 10 ? true : false;
						int small_id = id / 10;
						int big_id = id % 10;
						int targetId = getDetectedObjectIndex (big_id);

						if (targetId == -1 
							|| targetObjs.Length <= targetId
							|| ARGameObjects_SkyBox.Length <= targetId
							|| detected_indicies.Contains(targetId)) {
							continue;
						}

						GameObject targetObj = targetObjs [targetId];
						GameObject skyBoxObj = ARGameObjects_SkyBox [targetId];
						detected_indicies.Add (targetId);

						Aruco.drawAxis (rgbMat, camMatrix, distCoeffs, rvecs, tvecs, markerLength * 0.5f);
						double[] tvec = tvecs.get (i, 0);
						double[] rv = rvecs.get (i, 0);
						Mat rvec = new Mat (3, 1, CvType.CV_64FC1);
						rvec.put (0, 0, rv [0]);
						rvec.put (1, 0, rv [1]);
						rvec.put (2, 0, rv [2]);
						Calib3d.Rodrigues (rvec, rotMat);
						Matrix4x4 transformationM = new Matrix4x4 (); // from OpenCV
						transformationM.SetRow (
							0, 
							new Vector4 (
								(float)rotMat.get (0, 0) [0],
								(float)rotMat.get (0, 1) [0],
								(float)rotMat.get (0, 2) [0],
								(float)tvec [0]));
						transformationM.SetRow (
							1,
							new Vector4 (
								(float)rotMat.get (1, 0) [0],
								(float)rotMat.get (1, 1) [0],
								(float)rotMat.get (1, 2) [0],
								(float)tvec [1]));
						transformationM.SetRow (
							2, 
							new Vector4 (
								(float)rotMat.get (2, 0) [0], 
								(float)rotMat.get (2, 1) [0],
								(float)rotMat.get (2, 2) [0],
								(float)tvec [2]));
						transformationM.SetRow (
							3,
							new Vector4 (0, 0, 0, 1));
						Matrix4x4 invertZM 
						= Matrix4x4.TRS (Vector3.zero, Quaternion.identity, new Vector3 (1, 1, -1));
						Matrix4x4 invertYM 
						= Matrix4x4.TRS (Vector3.zero, Quaternion.identity, new Vector3 (1, -1, 1));
						Matrix4x4 ARM = invertYM * transformationM;
						ARM = ARM * invertZM;

						ARM = ARCamera.transform.localToWorldMatrix * ARM;
						ARUtils.SetTransformFromMatrix (targetObj.transform, ref ARM);

						calcuMovingObject2SkyBox (big_id, targetObj, skyBoxObj, small_id);
					}
				}
			}

			if (IS_SHOW_GIMIC_IN_SEARCHING) {
				if (showRejected && rejected.Count > 0) {
					Aruco.drawDetectedMarkers (rgbMat, rejected, new Mat (), new Scalar (0, 0, 255));
				}
				videoTexture = new Texture2D (rgbMat.cols (), rgbMat.rows (), TextureFormat.RGBA32, false);
				Utils.matToTexture2D (rgbMat, videoTexture);
				ReplaceTexture (gameObject.GetComponent<Renderer> ().material, videoTexture);
				if (skyBox != null) {
					ReplaceTexture (skyBox.GetComponent<Renderer> ().material, videoTexture);
				}
			}
				
			for (int i = 0; i < corners.Count; i++) {
				corners [i].release ();
			}
			for (int i = 0; i < rejected.Count; i++) {
				rejected [i].release ();
			}
			ids.release ();
			rvecs.release ();
			tvecs.release ();
			rotMat.release ();

			isShownDebugLogs = true;
			videoTexture = null;
			rgbMat.release ();

			isShownDebugLogs = true;
			videoTexture = null;
		}

		int getDetectedObjectIndex(int id) {
			for(int i = 0; i < targetIds.Length; i++) {
				if(targetIds[i] == id) {
					return i;
				}
			}
			return -1;
		}

		void procMovingObjectFromFile(int id, GameObject skyBoxObj) {
			FileIO.Data data_now = fileIO.getData (id, frame_count);
			FileIO.Data data_feature = fileIO.getFeatureData (id, frame_count, MODIFING_FRAME_DURATION);
			FileIO.Data data_past = fileIO.getPastData (id, frame_count, MODIFING_FRAME_DURATION);
			FileIO.Data lastData = lastDatas[id];

			if (data_now != null) {
				FileIO.Data ava_data = data_now;
				if (data_feature != null) {
					ava_data += data_feature;
					if (data_past != null) {
						ava_data += data_past;
						ava_data /= 3;
					} else {
						ava_data /= 2;
					}
				}
				if (lastData != null) {
					ava_data = FileIO.Data.Lerp (lastData, ava_data, LOCOMOTION_SENSITIVITY);
					//ava_data += lastData;
					//ava_data /= 2;
				}
				setSkyBoxObjPosition (skyBoxObj, ava_data.pos, ava_data.far_scale);
				lastDatas[id] = ava_data;
			} else {
				if (data_feature != null && data_past != null) {
					FileIO.Data data = fileIO.calculatingLerp (data_past, data_feature, frame_count);
					if (lastData != null) {
						data = FileIO.Data.Lerp (lastData, data, LOCOMOTION_SENSITIVITY);
						//data += lastData;
						//data /= 2;
					}
					setSkyBoxObjPosition (skyBoxObj, data.pos, data.far_scale);
					lastDatas[id] = data;
				} else {
					skyBoxObj.transform.position = GIMIC_NEST.transform.position;
					lastDatas[id] = null;
				}
			}
		}

		void calcuMovingObject2SkyBox(int id, GameObject target, GameObject skyboxObj, int small_id) {
			if (target == null)
				return;

			Vector3 target_ = target.transform.position;
			{
				Color debug_arrow_color = new Color (0, 0, 0, 0);
				Vector3 adjust_vector = new Vector3 (130, 322, 0);
				switch (small_id) {
				case 1:
					adjust_vector = new Vector3 (130, 322, 0);
					adjust_vector = target.transform.rotation * adjust_vector;
					debug_arrow_color = new Color (1, 0, 0, 1);
					break;
				case 2:
					adjust_vector = new Vector3 (-130, 322, 0);
					adjust_vector = target.transform.rotation * adjust_vector;
					debug_arrow_color = new Color (0, 1, 0, 1);
					break;
				default:
					adjust_vector = new Vector3 (0, 0, 0);
					debug_arrow_color = new Color (1, 1, 1, 1);
					break;
				}
				target_ += adjust_vector;
				if (debug_arrow != null) {
					debug_arrow.transform.rotation = target.transform.rotation;
					debug_arrow.GetComponent<Renderer> ().material.color = debug_arrow_color;
				}
			}

			Vector3 pos = ARCamera.WorldToScreenPoint (target_);
			float fw = (float)frameWidth;
			float fh = (float)frameHeight;
			float f_ratio = fw / fh;

			float far_scale_calibration = small_id != 0 ? 4f : 1f;
			float far_scale = target.transform.position.z / (fw * far_scale_calibration);

			Vector3 final_pos 
			= new Vector3(
				(pos.x / (float)ARCamera.pixelWidth - 0.5f) * 2f,
				(pos.y / (float)ARCamera.pixelWidth - 0.5f) * f_ratio * 2f,
				0);
			final_pos.x *= POINT_CARIBRATION_MULTIPLE.x;
			final_pos.y *= POINT_CARIBRATION_MULTIPLE.y;
			final_pos.x += POINT_CARIBRATION_ADD.x;
			final_pos.y += POINT_CARIBRATION_ADD.y;
			final_pos.x = Mathf.Max(-1.0f, Mathf.Min (1.0f, final_pos.x));
			final_pos.y = Mathf.Max(-1.0f, Mathf.Min (1.0f, final_pos.y));

			float R = skyBox.transform.localScale.x / (2*SkyBoxScaleFactor);
			float l = -final_pos.x * Mathf.PI;
			float s = final_pos.y;
			float x = R * Mathf.Cos (Mathf.Asin (s)) * Mathf.Cos (l);
			float y = R * Mathf.Cos (Mathf.Asin (s)) * Mathf.Sin (l);
			float z = s * R;

			Vector3 pos_result 
			= Vector3.Lerp (
				new Vector3 (x, z, y) + skyBox.transform.position, 
				USER.transform.position, 
				distance_ratio - (SORT_ORDER_RESPECT_FAR_SCALE * far_scale));
			fileIO.addData(id, frame_count, pos_result, far_scale);

			if (IS_SHOW_GIMIC_IN_SEARCHING) {
				setSkyBoxObjPosition (skyboxObj, pos_result, far_scale);
			}

			showDebug ("target rotation : " + target.transform.rotation.eulerAngles);
			showDebug ("ARCamera Pixcel Size : " + ARCamera.pixelWidth + "," + ARCamera.pixelHeight);
			showDebug ("ARCamera Aspect : " + ARCamera.aspect);
			showDebug ("procMovingObject2SkyBox : " + Mathf.Cos (l / R) + "," + Mathf.Sin (l / R));
			showDebug ("l, s : " + l + "," + s);
			showDebug ("x,y,z : " + x + "," + y + "," + z);
		}

		void setSkyBoxObjPosition(GameObject skyboxObj, Vector3 pos, float far_scale) {
			skyboxObj.transform.position = pos;
			skyboxObj.transform.localScale 
			= new Vector3 (MAX_SIZE / far_scale, MAX_SIZE / far_scale, MAX_SIZE / far_scale);
			skyboxObj.transform.LookAt (USER.transform.position);
		}


		void Start ()
		{
			loadVideo ();
			foreach (GameObject obj in ARGameObjects_SkyBox) {
				obj.transform.position = GIMIC_NEST.transform.position;
			}
		}

		void Update ()
		{
			if (is_finished) {
				if (IS_SERCHING_MODE) {
					UnityEditor.EditorApplication.isPlaying = false;
				}
				return;
			}
			if (frame_count >= total_frame_count-1) {
				is_finished = true;
				fileIO.write2file ();
			}
			if (IS_AUTO_PLAY) {
				RenderNextFrame ();
				if (indicator_text != null) {
					indicator_text.text = "time : " + frame_count + " / " + total_frame_count;
				}
			}
		}

		public void RenderNextFrame() {
			getVideoTexture ();
			if (IS_USED_FRAME_START_END && (frame_count < START_FRAME || frame_count > END_FRAME)) {
				int count = 0;
				do {
					getVideoTexture ();
					count++;
				} while((frame_count < START_FRAME || frame_count > END_FRAME) 
					&& count <= 100
					&& frame_count <= total_frame_count);
				return;
			}
			UpdateARObject ();
		}

		public void OnBackButtonClick ()
		{
			SceneManager.LoadScene ("OpenCVForUnityExample");
		}

		void ReplaceTexture(Material material, Texture2D texture) {
			MonoBehaviour.Destroy (material.mainTexture);
			material.mainTexture = texture;
		}
	}
}