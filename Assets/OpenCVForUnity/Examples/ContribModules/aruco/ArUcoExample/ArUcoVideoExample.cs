﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif
using OpenCVForUnity;

namespace OpenCVForUnityExample
{
    public class ArUcoVideoExample : MonoBehaviour
    {
        public Texture2D imgTexture;
        public int dictionaryId = 10;
        public bool showRejected = true;
        public bool applyEstimationPose = true;
        public float markerLength = 100;
        public GameObject[] ARGameObjects;
        public Camera ARCamera;
        public bool shouldMoveARCamera = false;

		[SerializeField]
		Vector2 point_calibration = new Vector2 (0, 0);
		[SerializeField]
		float SkyBoxScaleFactor = 4.1f;
		[SerializeField]
		GameObject ARGameObject_SkyBox;
		[SerializeField]
		float MIN_SIZE = 0.01f;
		[SerializeField]
		float MAX_SIZE = 1.0f;
		[SerializeField]
		float distance_ratio = 0.2f;
		[SerializeField]
		GameObject skyBox;
		[SerializeField]
		string VIDEO_FILE_PATH = "";
		[SerializeField]
		GameObject USER;
		[SerializeField]
		bool IS_AUTO_PLAY = true;
		public int frame_count = 0;
		public int total_frame_count = 0;

		float width, height;
		Mat camMatrix;
		MatOfDouble distCoeffs;


		Texture2D videoTexture;
		VideoCapture videoCapture;
		Mat originalMat;
		//Mat grayMat;
		//float video_width, video_height;
		float imageSizeScale = 1.0f;
		float widthScale, heightScale;
		//Color32[] colorSpace;
		bool isShownDebugLogs = false;
		float original_y_size;
		int frameWidth = 0;
		int frameHeight = 0;

		void loadVideo() {
			videoCapture = new VideoCapture ();
			videoCapture.open (Utils.getFilePath (VIDEO_FILE_PATH));

			if (!videoCapture.isOpened ()) {
				Debug.LogError ("video catpure open error");
			}

			originalMat = new Mat ();
			total_frame_count = (int)videoCapture.get(Videoio.CAP_PROP_FRAME_COUNT);
			videoCapture.grab ();
			videoCapture.retrieve (originalMat, 0);
			frameWidth = originalMat.cols ();
			frameHeight = originalMat.rows ();

			width = originalMat.width ();
			height = originalMat.height ();

			originalMat.release ();
		}

		void getVideoTexture() {
			if (videoCapture.isOpened ()) {
				videoCapture.grab ();
				videoCapture.retrieve (originalMat, 0);
			}
		}

		void showDebug(string str) {
			if (!isShownDebugLogs) {
				Debug.Log (str);
			}
		}

		void InitProc() {
		}

		void UpdateARObject() {
			Mat rgbMat = new Mat();
			Mat rvecs = new Mat ();
			Mat tvecs = new Mat ();
			Mat rotMat = new Mat (3, 3, CvType.CV_64FC1);
			Mat ids = new Mat ();
			List<Mat> corners = new List<Mat> ();
			List<Mat> rejected = new List<Mat> ();



			//colorSpace = new Color32[frameWidth * frameHeight];
			videoTexture = new Texture2D (frameWidth, frameHeight, TextureFormat.RGB24, false);
			gameObject.transform.localScale = new Vector3 ((float)frameWidth, (float)frameHeight, 1);

			float skybox_scale = (((float)frameWidth) / (Mathf.PI)) * SkyBoxScaleFactor;
			skyBox.transform.localScale = new Vector3 (skybox_scale, -skybox_scale, -skybox_scale);
			original_y_size = (float)frameHeight;

			widthScale = (float)Screen.width / (float)frameWidth;
			heightScale = (float)Screen.height / (float)frameHeight;
			if (widthScale < heightScale) {
				//Camera.main.orthographicSize = ((float)frameWidth * (float)Screen.height / (float)Screen.width) / 2;
				imageSizeScale = (float)Screen.height / (float)Screen.width;
			} else {
				//Camera.main.orthographicSize = (float)frameHeight / 2;
			}

			videoCapture.set (Videoio.CAP_PROP_POS_FRAMES, 0);
			gameObject.GetComponent<Renderer> ().material.mainTexture = videoTexture;


			int max_d = (int)Mathf.Max (width, height);
			double fx = max_d;
			double fy = max_d;
			double cx = width / 2.0f;
			double cy = height / 2.0f;
			camMatrix = new Mat (3, 3, CvType.CV_64FC1);
			camMatrix.put (0, 0, fx);
			camMatrix.put (0, 1, 0);
			camMatrix.put (0, 2, cx);
			camMatrix.put (1, 0, 0);
			camMatrix.put (1, 1, fy);
			camMatrix.put (1, 2, cy);
			camMatrix.put (2, 0, 0);
			camMatrix.put (2, 1, 0);
			camMatrix.put (2, 2, 1.0f);
			showDebug ("camMatrix " + camMatrix.dump ());

			distCoeffs = new MatOfDouble (0, 0, 0, 0);

			showDebug ("distCoeffs " + distCoeffs.dump ());

			Size imageSize = new Size (width * imageSizeScale, height * imageSizeScale);
			double apertureWidth = 0;
			double apertureHeight = 0;
			double[] fovx = new double[1];
			double[] fovy = new double[1];
			double[] focalLength = new double[1];
			Point principalPoint = new Point (0, 0);
			double[] aspectratio = new double[1];

			Calib3d.calibrationMatrixValues (
				camMatrix, 
				imageSize, 
				apertureWidth, 
				apertureHeight, 
				fovx, 
				fovy, 
				focalLength, 
				principalPoint, 
				aspectratio);

			showDebug ("imageSize " + imageSize.ToString ());
			showDebug ("apertureWidth " + apertureWidth);
			showDebug ("apertureHeight " + apertureHeight);
			showDebug ("fovx " + fovx [0]);
			showDebug ("fovy " + fovy [0]);
			showDebug ("focalLength " + focalLength [0]);
			showDebug ("principalPoint " + principalPoint.ToString ());
			showDebug ("aspectratio " + aspectratio [0]);

			double fovXScale 
			= (2.0 * Mathf.Atan (
				(float)(imageSize.width / (2.0 * fx)))) 
				/ (Mathf.Atan2 ((float)cx, (float)fx) + Mathf.Atan2 ((float)(imageSize.width - cx), (float)fx));
			double fovYScale 
			= (2.0 * Mathf.Atan (
				(float)(imageSize.height / (2.0 * fy)))) 
				/ (Mathf.Atan2 ((float)cy, (float)fy) + Mathf.Atan2 ((float)(imageSize.height - cy), (float)fy));

			showDebug ("fovXScale " + fovXScale);
			showDebug ("fovYScale " + fovYScale);


			if (widthScale < heightScale) {
				ARCamera.fieldOfView = (float)(fovx [0] * fovXScale);
			} else {
				ARCamera.fieldOfView = (float)(fovy [0] * fovYScale);
			}




			Imgproc.cvtColor (originalMat, rgbMat, Imgproc.COLOR_BGR2RGB);
			DetectorParameters detectorParams = DetectorParameters.create ();
			Dictionary dictionary = Aruco.getPredefinedDictionary (dictionaryId);
			Aruco.detectMarkers (rgbMat, dictionary, corners, ids, detectorParams, rejected, camMatrix, distCoeffs);

			if (applyEstimationPose && ids.total () > 0)
				Aruco.estimatePoseSingleMarkers (corners, markerLength, camMatrix, distCoeffs, rvecs, tvecs);

			if (ids.total () > 0) {
				Aruco.drawDetectedMarkers (rgbMat, corners, ids, new Scalar (0, 255, 0));
				//applyEstimationPose = true;

				if (applyEstimationPose) {
					for (int i = 0; i < ids.total(); i++) {

						showDebug ("MarkerLength : " + markerLength);
						Aruco.drawAxis (rgbMat, camMatrix, distCoeffs, rvecs, tvecs, markerLength * 0.5f);

						if (ARGameObjects.Length >= (i+1) && ARGameObjects[i] != null) {
							double[] tvec = tvecs.get (i, 0);
							double[] rv = rvecs.get (i, 0);
							Mat rvec = new Mat (3, 1, CvType.CV_64FC1);
							rvec.put (0, 0, rv [0]);
							rvec.put (1, 0, rv [1]);
							rvec.put (2, 0, rv [2]);
							Calib3d.Rodrigues (rvec, rotMat);

							Matrix4x4 transformationM = new Matrix4x4 (); // from OpenCV
							transformationM.SetRow (
								0, 
								new Vector4 (
									(float)rotMat.get (0, 0) [0],
									(float)rotMat.get (0, 1) [0],
									(float)rotMat.get (0, 2) [0],
									(float)tvec [0]));
							transformationM.SetRow (
								1,
								new Vector4 (
									(float)rotMat.get (1, 0) [0],
									(float)rotMat.get (1, 1) [0],
									(float)rotMat.get (1, 2) [0],
									(float)tvec [1]));
							transformationM.SetRow (
								2, 
								new Vector4 (
									(float)rotMat.get (2, 0) [0], 
									(float)rotMat.get (2, 1) [0],
									(float)rotMat.get (2, 2) [0],
									(float)tvec [2]));
							transformationM.SetRow (
								3,
								new Vector4 (0, 0, 0, 1));
							showDebug ("transformationM " + transformationM.ToString ());

							Matrix4x4 invertZM 
							= Matrix4x4.TRS (Vector3.zero, Quaternion.identity, new Vector3 (1, 1, -1));
							showDebug ("invertZM " + invertZM.ToString ());

							Matrix4x4 invertYM 
							= Matrix4x4.TRS (Vector3.zero, Quaternion.identity, new Vector3 (1, -1, 1));
							showDebug ("invertYM " + invertYM.ToString ());

							Matrix4x4 ARM = invertYM * transformationM;
							ARM = ARM * invertZM;

							if (shouldMoveARCamera) {
								ARM = ARGameObjects[i].transform.localToWorldMatrix * ARM.inverse;
								showDebug ("ARM " + ARM.ToString ());
								ARUtils.SetTransformFromMatrix (ARCamera.transform, ref ARM);
							} else {
								ARM = ARCamera.transform.localToWorldMatrix * ARM;
								showDebug ("ARM " + ARM.ToString ());
								ARUtils.SetTransformFromMatrix (ARGameObjects[i].transform, ref ARM);

								showDebug("position : " + i + " = " + ARGameObjects[i].transform.position);

								procMovingObject2SkyBox (ARGameObjects [i]);
							}

							rvec.release ();
						}
					}
				}
			}

			if (showRejected && rejected.Count > 0)
				Aruco.drawDetectedMarkers (rgbMat, rejected, new Mat (), new Scalar (0, 0, 255));

			videoTexture = new Texture2D (rgbMat.cols (), rgbMat.rows (), TextureFormat.RGBA32, false);
			Utils.matToTexture2D (rgbMat, videoTexture);
			gameObject.GetComponent<Renderer> ().material.mainTexture = videoTexture;
			if (skyBox != null) {
				Texture2D videoTexture_without_ArucoInfo = new Texture2D (originalMat.cols (), originalMat.rows (), TextureFormat.RGBA32, false);
				Imgproc.cvtColor (originalMat, originalMat, Imgproc.COLOR_BGR2RGB);
				Utils.matToTexture2D (originalMat, videoTexture_without_ArucoInfo);
				skyBox.GetComponent<Renderer> ().material.mainTexture = videoTexture_without_ArucoInfo;
				frame_count++;
				videoTexture_without_ArucoInfo = null;
			}
				

			for (int i = 0; i < corners.Count; i++) {
				corners [i].release ();
			}
			for (int i = 0; i < rejected.Count; i++) {
				rejected [i].release ();
			}
			ids.release ();
			rvecs.release ();
			tvecs.release ();
			rotMat.release ();
			rgbMat.release ();
			originalMat.release ();
			camMatrix.release ();
			isShownDebugLogs = true;
			videoTexture = null;
		}
			

		void procMovingObject2SkyBox(GameObject target) {
			if (target == null)
				return;

			Vector3 pos = ARCamera.WorldToScreenPoint (target.transform.position);
			float fw = (float)frameWidth;
			float fh = (float)frameHeight;

			Vector3 ratio_camera_frame = new Vector2 (fw / ARCamera.pixelWidth, fh / ARCamera.pixelHeight);

			Vector3 final_pos 
			= new Vector3(
				(pos.x - ARCamera.pixelWidth/2 + point_calibration.x) * ratio_camera_frame.x, 
				(pos.y / ARCamera.aspect - ARCamera.pixelHeight/2 + point_calibration.y) * ratio_camera_frame.y, 
				0);

			float R = skyBox.transform.localScale.x / (2*SkyBoxScaleFactor);
			float l = -final_pos.x;
			float s = final_pos.y * ((2*R) / original_y_size);
			float x = R * Mathf.Cos (Mathf.Asin (s / R)) * Mathf.Cos (l / R);
			float y = R * Mathf.Cos (Mathf.Asin (s / R)) * Mathf.Sin (l / R);
			float z = s;

			ARGameObject_SkyBox.transform.position = Vector3.Lerp (new Vector3 (x, z, y) + skyBox.transform.position, USER.transform.position, distance_ratio);
			float far_scale = target.transform.position.z / fw;
			ARGameObject_SkyBox.transform.localScale = new Vector3 (MAX_SIZE / far_scale, MAX_SIZE / far_scale, MAX_SIZE / far_scale);
			ARGameObject_SkyBox.transform.LookAt (USER.transform.position);

			showDebug ("ARCamera Pixcel Size : " + ARCamera.pixelWidth + "," + ARCamera.pixelHeight);
			showDebug ("procMovingObject2SkyBox : " + Mathf.Cos (l / R) + "," + Mathf.Sin (l / R));
			showDebug ("l, s : " + l + "," + s);
			showDebug ("x,y,z : " + x + "," + y + "," + z);
		}
        

        void Start ()
        {
			loadVideo ();
        }
        
        void Update ()
		{
			if (IS_AUTO_PLAY) {
				RenderNextFrame ();
			}
        }

		public void RenderNextFrame() {
			getVideoTexture ();
			UpdateARObject ();
		}
			
        public void OnBackButtonClick ()
        {
            SceneManager.LoadScene ("OpenCVForUnityExample");
        }
    }
}