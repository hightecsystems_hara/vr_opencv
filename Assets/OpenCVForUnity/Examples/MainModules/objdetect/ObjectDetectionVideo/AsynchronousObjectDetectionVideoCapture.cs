﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif
using OpenCVForUnity;
using Rect = OpenCVForUnity.Rect;
using PositionsVector = System.Collections.Generic.List<OpenCVForUnity.Rect>;

namespace OpenCVForUnityExample
{
    /// <summary>
    /// Asynchronous Face Detection WebCamTexture Example
    /// Referring to https://github.com/Itseez/opencv/blob/master/modules/objdetect/src/detection_based_tracker.cpp.
    /// </summary>
	public class AsynchronousObjectDetectionVideoCapture : MonoBehaviour
    {
		public string SMALL_CASCADE_FILE_FOLDER = "cascades";
		public string LARGE_CASCADE_ALT_FILE_FOLDER = "cascades";
		public string VIDEO_FILE_PATH = "768x576_mjpeg.mjpeg";

        Mat grayMat;
        Texture2D texture;
		VideoCapture videoCapture;
        CascadeClassifier[] cascades = null;

		float large_image_min_size_factor = 0.1f;

		//string[] lbpcascade_video_xml_filepaths;
		//string[] haarcascade_video_alt_xml_filepaths;
		string[] large_region_xml_filepaths;
		string[] small_region_xml_filepaths;

		Color32[] colors;
        Rect[] rectsWhereRegions;

        List<Rect> detectedObjectsInRegions = new List<Rect> ();
        List<Rect> resultObjects = new List<Rect> ();

        CascadeClassifier[] cascade4Threads = null;
        Mat grayMat4Thread;
        MatOfRect[] detectionResults;
        System.Object sync = new System.Object ();

        bool _isThreadRunning = false;

        bool isThreadRunning {
            get {
                lock (sync)
                    return _isThreadRunning;
            }
            set {
                lock (sync)
                    _isThreadRunning = value;
            }
        }

        bool _shouldStopThread = false;

        bool shouldStopThread {
            get {
                lock (sync)
                    return _shouldStopThread;
            }
            set {
                lock (sync)
                    _shouldStopThread = value;
            }
        }

        bool _shouldDetectInMultiThread = false;

        bool shouldDetectInMultiThread {
            get {
                lock (sync)
                    return _shouldDetectInMultiThread;
            }
            set {
                lock (sync)
                    _shouldDetectInMultiThread = value;
            }
        }

        bool _didUpdateTheDetectionResult = false;

        bool didUpdateTheDetectionResult {
            get {
                lock (sync)
                    return _didUpdateTheDetectionResult;
            }
            set {
                lock (sync)
                    _didUpdateTheDetectionResult = value;
            }
        }

        // for tracker
        List<TrackedObject> trackedObjects = new List<TrackedObject> ();
        List<float> weightsPositionsSmoothing = new List<float> ();
        List<float> weightsSizesSmoothing = new List<float> ();
        Parameters parameters;
        InnerParameters innerParameters;

        // Use this for initialization
        void Start ()
        {
			small_region_xml_filepaths = Utils.getFilesInDirectory (SMALL_CASCADE_FILE_FOLDER, ".xml");
			large_region_xml_filepaths = Utils.getFilesInDirectory (LARGE_CASCADE_ALT_FILE_FOLDER, ".xml");

			videoCapture = new VideoCapture ();
			videoCapture.open (Utils.getFilePath (VIDEO_FILE_PATH));

            Run ();
        }
		

        private void Run ()
        {
            weightsPositionsSmoothing.Add (1);
            weightsSizesSmoothing.Add (0.5f);
            weightsSizesSmoothing.Add (0.3f);
            weightsSizesSmoothing.Add (0.2f);
            
            parameters.maxTrackLifetime = 5;
            
            innerParameters.numLastPositionsToTrack = 4;
            innerParameters.numStepsToWaitBeforeFirstShow = 6;
            innerParameters.numStepsToTrackWithoutDetectingIfObjectHasNotBeenShown = 3;
            innerParameters.numStepsToShowWithoutDetecting = 3;
            innerParameters.coeffTrackingWindowSize = 2.0f;
            innerParameters.coeffObjectSizeToTrack = 0.85f;
            innerParameters.coeffObjectSpeedUsingInPrediction = 0.8f;
		
			OnVideoTextureToMatInitialized ();
        }


		public void OnVideoTextureToMatInitialized() {
			Mat videoMat = new Mat ();
			if (!videoCapture.isOpened ()) {
				Debug.LogError 
				("capture.isOpened() is false. " +
					"Please copy from “OpenCVForUnity/StreamingAssets/” to “Assets/StreamingAssets/” folder. ");
			}

			//double ext = videoCapture.get (Videoio.CAP_PROP_FOURCC);
			videoCapture.grab ();
			videoCapture.retrieve (videoMat, 0);
			int frameWidth = videoMat.cols ();
			int frameHeight = videoMat.rows ();
			colors = new Color32[frameWidth * frameHeight];
			texture = new Texture2D (frameWidth, frameHeight, TextureFormat.RGB24, false);
			gameObject.transform.localScale = new Vector3 ((float)frameWidth, (float)frameHeight, 1);
			float widthScale = (float)Screen.width / (float)frameWidth;
			float heightScale = (float)Screen.height / (float)frameHeight;
			if (widthScale < heightScale) {
				Camera.main.orthographicSize = ((float)frameWidth * (float)Screen.height / (float)Screen.width) / 2;
			} else {
				Camera.main.orthographicSize = (float)frameHeight / 2;
			}

			videoCapture.set (Videoio.CAP_PROP_POS_FRAMES, 0);
			gameObject.GetComponent<Renderer> ().material.mainTexture = texture;

			grayMat = new Mat (videoMat.rows (), videoMat.cols (), CvType.CV_8UC1);
			cascades = new CascadeClassifier[small_region_xml_filepaths.Length];
			for (int i = 0; i < small_region_xml_filepaths.Length; i++) {
				cascades[i] = new CascadeClassifier ();
				cascades[i].load (small_region_xml_filepaths[i]);
			}
			InitThread ();
		}
			

		public void OnVideoTextureToMatDisposed() {
			StopThread ();
			StopCoroutine ("ThreadWorker");
			if (grayMat4Thread != null)
				grayMat4Thread.Dispose ();

			if (cascade4Threads != null) {
				foreach (CascadeClassifier cas in cascade4Threads) {
					cas.Dispose ();
				}
			}

			if (grayMat != null)
				grayMat.Dispose ();
			if (cascades != null) {
				foreach (CascadeClassifier cas in cascades) {
					cas.Dispose ();
				}
			}

			trackedObjects.Clear ();
		}
			

        public void OnWebCamTextureToMatHelperErrorOccurred (WebCamTextureToMatHelper.ErrorCode errorCode)
        {
            Debug.Log ("OnWebCamTextureToMatHelperErrorOccurred " + errorCode);
        }

        // Update is called once per frame
        void Update ()
        {
			if (videoCapture.isOpened()) {
                
				Mat rgbaMat = new Mat ();
				videoCapture.grab ();
				videoCapture.retrieve (rgbaMat, 0);
				Imgproc.cvtColor (rgbaMat, rgbaMat, Imgproc.COLOR_BGR2RGB);

                Imgproc.cvtColor (rgbaMat, grayMat, Imgproc.COLOR_RGBA2GRAY);
                Imgproc.equalizeHist (grayMat, grayMat);

                if (!shouldDetectInMultiThread) {
                    grayMat.copyTo (grayMat4Thread);

                    shouldDetectInMultiThread = true;
                }

                OpenCVForUnity.Rect[] rects;
                
                if (didUpdateTheDetectionResult) {
                    didUpdateTheDetectionResult = false;
					List<Rect> results = new List<Rect> ();
					if (detectionResults != null) {
						foreach (MatOfRect mor in detectionResults) {
							if (mor != null) {
								results.AddRange (mor.toArray ());
							}
						}
					}
					rectsWhereRegions = results.ToArray ();
                
                    rects = rectsWhereRegions;
                    for (int i = 0; i < rects.Length; i++) {
                        Imgproc.rectangle (rgbaMat, 
							new Point (rects [i].x, rects [i].y), 
							new Point (rects [i].x + rects [i].width, rects [i].y + rects [i].height), 
							new Scalar (0, 0, 255, 255), 2);
                    }
                } else {
                    //Debug.Log("DetectionBasedTracker::process: get _rectsWhereRegions from previous positions");
                    rectsWhereRegions = new Rect[trackedObjects.Count];
                
                    for (int i = 0; i < trackedObjects.Count; i++) {
                        int n = trackedObjects [i].lastPositions.Count;
                        Rect r = trackedObjects [i].lastPositions [n - 1].clone ();
                        if (r.area () == 0) {
                            Debug.Log (
								"DetectionBasedTracker::process: " +
								"ERROR: ATTENTION: strange algorithm's behavior: trackedObjects[i].rect() is empty");
                            continue;
                        }
                
                        //correction by speed of rectangle
                        if (n > 1) {
                            Point center = CenterRect (r);
                            Point center_prev = CenterRect (trackedObjects [i].lastPositions [n - 2]);
                            Point shift = 
								new Point (
									(center.x - center_prev.x) * innerParameters.coeffObjectSpeedUsingInPrediction,
                                	(center.y - center_prev.y) * innerParameters.coeffObjectSpeedUsingInPrediction);
                
                            r.x += (int)Math.Round (shift.x);
                            r.y += (int)Math.Round (shift.y);
                        }
                        rectsWhereRegions [i] = r;
                    }

                    rects = rectsWhereRegions;
                    for (int i = 0; i < rects.Length; i++) {
                        Imgproc.rectangle (rgbaMat, 
							new Point (rects [i].x, rects [i].y), 
							new Point (rects [i].x + rects [i].width, rects [i].y + rects [i].height), 
							new Scalar (0, 255, 0, 255), 2);
                    }
                }

                detectedObjectsInRegions.Clear ();
                if (rectsWhereRegions.Length > 0) {
                    int len = rectsWhereRegions.Length;
                    for (int i = 0; i < len; i++) {
                        DetectInRegion (grayMat, rectsWhereRegions [i], detectedObjectsInRegions);
                    }
                }

                UpdateTrackedObjects (detectedObjectsInRegions);
                GetObjects (resultObjects);

                rects = resultObjects.ToArray ();
                for (int i = 0; i < rects.Length; i++) {
                    Imgproc.rectangle (rgbaMat, 
						new Point (rects [i].x, rects [i].y), 
						new Point (rects [i].x + rects [i].width, rects [i].y + rects [i].height), 
						new Scalar (255, 0, 0, 255), 2);
                }
					
				Utils.matToTexture2D (rgbaMat, texture, colors);
            }
        }

        private void DetectInRegion (Mat img, Rect r, List<Rect> detectedObjectsInRegions)
        {
            Rect r0 = new Rect (new Point (), img.size ());
            Rect r1 = new Rect (r.x, r.y, r.width, r.height);
            Rect.inflate (r1, (int)((r1.width * innerParameters.coeffTrackingWindowSize) - r1.width) / 2,
                (int)((r1.height * innerParameters.coeffTrackingWindowSize) - r1.height) / 2);
            r1 = Rect.intersect (r0, r1);
            
            if (r1 != null && (r1.width <= 0) || (r1.height <= 0)) {
                Debug.Log ("DetectionBasedTracker::detectInRegion: Empty intersection");
                return;
            }

            int d = Math.Min (r.width, r.height);
			d = (int)Math.Round (d * innerParameters.coeffObjectSizeToTrack);

            MatOfRect tmpobjects = new MatOfRect ();
            Mat img1 = new Mat (img, r1);//subimage for rectangle -- without data copying
            
			foreach (CascadeClassifier cas in cascades) {
				cas
				.detectMultiScale (img1, 
					tmpobjects, 1.1, 2, 
					0 | Objdetect.CASCADE_DO_CANNY_PRUNING | Objdetect.CASCADE_SCALE_IMAGE | Objdetect.CASCADE_FIND_BIGGEST_OBJECT, 
					new Size (d, d), new Size ());
				Rect[] tmpobjectsArray = tmpobjects.toArray ();
				int len = tmpobjectsArray.Length;
				for (int i = 0; i < len; i++) {
					Rect tmp = tmpobjectsArray [i];
					Rect curres = new Rect (new Point (tmp.x + r1.x, tmp.y + r1.y), tmp.size ());
					detectedObjectsInRegions.Add (curres);
				}
			}
        }

        public Point CenterRect (Rect r)
        {
            return new Point (r.x + (r.width / 2), r.y + (r.height / 2));
        }

        private void InitThread ()
        {
            StopThread ();
            
            grayMat4Thread = new Mat ();
            
			cascade4Threads = new CascadeClassifier[large_region_xml_filepaths.Length];
			for (int i = 0; i < large_region_xml_filepaths.Length; i++) {
				cascade4Threads[i] = new CascadeClassifier ();
				cascade4Threads[i].load (large_region_xml_filepaths[i]);
				#if !UNITY_WSA_10_0
				if (cascade4Threads[i].empty ()) {
					Debug.LogError ("cascade4Thread file is not loaded.Please copy from “OpenCVForUnity/StreamingAssets/” to “Assets/StreamingAssets/” folder. ");
				}
				#endif
			}	
            
            shouldDetectInMultiThread = false;
            StartThread (ThreadWorker);
        }

        private void StartThread (Action action)
        {
            shouldStopThread = false;

            #if UNITY_METRO && NETFX_CORE
            System.Threading.Tasks.Task.Run(() => action());
            #elif UNITY_METRO
            action.BeginInvoke(ar => action.EndInvoke(ar), null);
            #else
            ThreadPool.QueueUserWorkItem (_ => action ());
            #endif

            Debug.Log ("Thread Start");
        }

        private void StopThread ()
        {
            if (!isThreadRunning)
                return;

            shouldStopThread = true;

            while (isThreadRunning) {
                //Wait threading stop
            } 
            Debug.Log ("Thread Stop");
        }
			
        private void ThreadWorker ()
        {
            isThreadRunning = true;

            while (!shouldStopThread) {
                if (!shouldDetectInMultiThread)
                    continue;

                Detect ();

                shouldDetectInMultiThread = false;
                didUpdateTheDetectionResult = true;
            }

            isThreadRunning = false;
        }
			
        private void Detect ()
        {
			detectionResults = new MatOfRect[cascade4Threads.Length];
			for(int i = 0; i < cascade4Threads.Length; i++) {
				MatOfRect objects = new MatOfRect ();
				if (cascade4Threads[i] != null) {
					cascade4Threads[i].detectMultiScale (
						grayMat4Thread, objects, 1.1, 2, Objdetect.CASCADE_SCALE_IMAGE, 
						new Size (grayMat4Thread.height () * large_image_min_size_factor, grayMat4Thread.height () * large_image_min_size_factor), 
						new Size (grayMat4Thread.height (), grayMat4Thread.height ()));
					detectionResults[i] = objects;
				}
			}
        }
			
        void OnDestroy ()
        {
			videoCapture.release ();
			videoCapture.Dispose ();
        }
			
        public void OnBackButtonClick ()
        {
            #if UNITY_5_3 || UNITY_5_3_OR_NEWER
            SceneManager.LoadScene ("OpenCVForUnityExample");
            #else
            Application.LoadLevel ("OpenCVForUnityExample");
            #endif
        }
			
        public void OnPlayButtonClick ()
        {
            //webCamTextureToMatHelper.Play ();
        }
			
        public void OnPauseButtonClick ()
        {
            //webCamTextureToMatHelper.Pause ();
        }
			
        public void OnStopButtonClick ()
        {
            //webCamTextureToMatHelper.Stop ();
        }
			
        public void OnChangeCameraButtonClick ()
        {
			/*
            webCamTextureToMatHelper.Initialize (null, 
				webCamTextureToMatHelper.requestedWidth, 
				webCamTextureToMatHelper.requestedHeight, 
				!webCamTextureToMatHelper.requestedIsFrontFacing);
			*/
		}


        private void GetObjects (List<Rect> result)
        {
            result.Clear ();
                
            for (int i = 0; i < trackedObjects.Count; i++) {
                Rect r = CalcTrackedObjectPositionToShow (i);
                if (r.area () == 0) {
                    continue;
                }
                result.Add (r);
			}
        }

        private enum TrackedState : int
        {
            NEW_RECTANGLE = -1,
            INTERSECTED_RECTANGLE = -2
        }

        private void UpdateTrackedObjects (List<Rect> detectedObjects)
        {
            int N1 = (int)trackedObjects.Count;
            int N2 = (int)detectedObjects.Count;
                
            for (int i = 0; i < N1; i++) {
                trackedObjects [i].numDetectedFrames++;
            }
                
            int[] correspondence = new int[N2];
            for (int i = 0; i < N2; i++) {
                correspondence [i] = (int)TrackedState.NEW_RECTANGLE;
            }
                
            for (int i = 0; i < N1; i++) {
                TrackedObject curObject = trackedObjects [i];
                    
                int bestIndex = -1;
                int bestArea = -1;
                    
                int numpositions = (int)curObject.lastPositions.Count;

                Rect prevRect = curObject.lastPositions [numpositions - 1];
                    
                for (int j = 0; j < N2; j++) {
					if (correspondence [j] >= 0 || correspondence [j] != (int)TrackedState.NEW_RECTANGLE) {
						continue;
					}
                        
                    Rect r = Rect.intersect (prevRect, detectedObjects [j]);
                    if (r != null && (r.width > 0) && (r.height > 0)) {
						correspondence [j] = (int)TrackedState.INTERSECTED_RECTANGLE;
                            
                        if (r.area () > bestArea) {
							bestIndex = j;
                            bestArea = (int)r.area ();
                        }
                    }
                }
                    
                if (bestIndex >= 0) {
					correspondence [bestIndex] = i;
                        
                    for (int j = 0; j < N2; j++) {
                        if (correspondence [j] >= 0)
                            continue;
                            
                        Rect r = Rect.intersect (detectedObjects [j], detectedObjects [bestIndex]);
                        if (r != null && (r.width > 0) && (r.height > 0)) {
                            correspondence [j] = (int)TrackedState.INTERSECTED_RECTANGLE;
                        }
                    }
                } else {
 					curObject.numFramesNotDetected++;
                }
            }
                
            for (int j = 0; j < N2; j++) {
                int i = correspondence [j];
                if (i >= 0) {
                    trackedObjects [i].lastPositions.Add (detectedObjects [j]);
                    while ((int)trackedObjects [i].lastPositions.Count > (int)innerParameters.numLastPositionsToTrack) {
                        trackedObjects [i].lastPositions.Remove (trackedObjects [i].lastPositions [0]);
                    }
                    trackedObjects [i].numFramesNotDetected = 0;
                } else if (i == (int)TrackedState.NEW_RECTANGLE) { //new object
                    trackedObjects.Add (new TrackedObject (detectedObjects [j]));
                } else {
                }
            }
                
            int t = 0;
            TrackedObject it;
            while (t < trackedObjects.Count) {
                it = trackedObjects [t];
                if ((it.numFramesNotDetected > parameters.maxTrackLifetime)
                    ||
                    ((it.numDetectedFrames <= innerParameters.numStepsToWaitBeforeFirstShow)
                    &&
                    (it.numFramesNotDetected > innerParameters.numStepsToTrackWithoutDetectingIfObjectHasNotBeenShown))) {
                    trackedObjects.Remove (it);
                } else {
                    t++;
                }
            }
        }

        private Rect CalcTrackedObjectPositionToShow (int i)
        {
            if ((i < 0) || (i >= trackedObjects.Count)) {
                Debug.Log ("DetectionBasedTracker::calcTrackedObjectPositionToShow: ERROR: wrong i=" + i);
                return new Rect ();
            }
            if (trackedObjects [i].numDetectedFrames <= innerParameters.numStepsToWaitBeforeFirstShow) {
				return new Rect ();
            }
            if (trackedObjects [i].numFramesNotDetected > innerParameters.numStepsToShowWithoutDetecting) {
                return new Rect ();
            }
                
            List<Rect> lastPositions = trackedObjects [i].lastPositions;
                
            int N = lastPositions.Count;
            if (N <= 0) {
                Debug.Log ("DetectionBasedTracker::calcTrackedObjectPositionToShow: ERROR: no positions for i=" + i);
                return new Rect ();
            }
                
            int Nsize = Math.Min (N, (int)weightsSizesSmoothing.Count);
            int Ncenter = Math.Min (N, (int)weightsPositionsSmoothing.Count);
                
            Point center = new Point ();
            double w = 0, h = 0;
            if (Nsize > 0) {
                double sum = 0;
                for (int j = 0; j < Nsize; j++) {
                    int k = N - j - 1;
                    w += lastPositions [k].width * weightsSizesSmoothing [j];
                    h += lastPositions [k].height * weightsSizesSmoothing [j];
                    sum += weightsSizesSmoothing [j];
                }
                w /= sum;
                h /= sum;
            } else {
                w = lastPositions [N - 1].width;
                h = lastPositions [N - 1].height;
            }
                
            if (Ncenter > 0) {
                double sum = 0;
                for (int j = 0; j < Ncenter; j++) {
                    int k = N - j - 1;
                    Point tl = lastPositions [k].tl ();
                    Point br = lastPositions [k].br ();
                    Point c1;
                    c1 = new Point (tl.x * 0.5f, tl.y * 0.5f);
                    Point c2;
                    c2 = new Point (br.x * 0.5f, br.y * 0.5f);
                    c1 = new Point (c1.x + c2.x, c1.y + c2.y);
                        
                    center = new Point (
						center.x + (c1.x * weightsPositionsSmoothing [j]), 
						center.y + (c1.y * weightsPositionsSmoothing [j]));
                    sum += weightsPositionsSmoothing [j];
                }
                center = new Point (center.x * (1 / sum), center.y * (1 / sum));
            } else {
                int k = N - 1;
                Point tl = lastPositions [k].tl ();
                Point br = lastPositions [k].br ();
                Point c1;
                c1 = new Point (tl.x * 0.5f, tl.y * 0.5f);
                Point c2;
                c2 = new Point (br.x * 0.5f, br.y * 0.5f);
                    
                center = new Point (c1.x + c2.x, c1.y + c2.y);
            }
            Point tl2 = new Point (center.x - (w * 0.5f), center.y - (h * 0.5f));
            Rect res = new Rect ((int)Math.Round (tl2.x), 
				(int)Math.Round (tl2.y), (int)Math.Round (w), (int)Math.Round (h));

            return res;
        }

        private struct Parameters
        {
            public int maxTrackLifetime;
            //public int minDetectionPeriod; //the minimal time between run of the big object detector (on the whole frame) in ms (1000 mean 1 sec), default=0
        };

        private struct InnerParameters
        {
            public int numLastPositionsToTrack;
            public int numStepsToWaitBeforeFirstShow;
            public int numStepsToTrackWithoutDetectingIfObjectHasNotBeenShown;
            public int numStepsToShowWithoutDetecting;
            public float coeffTrackingWindowSize;
            public float coeffObjectSizeToTrack;
            public float coeffObjectSpeedUsingInPrediction;
        };

        private class TrackedObject
        {
            public PositionsVector lastPositions;
            public int numDetectedFrames;
            public int numFramesNotDetected;
            public int id;
            static private int _id = 0;

            public TrackedObject (OpenCVForUnity.Rect rect)
            {
                lastPositions = new PositionsVector ();

                numDetectedFrames = 1;
                numFramesNotDetected = 0;

                lastPositions.Add (rect.clone ());

                _id = GetNextId ();
                id = _id;
            }

            static int GetNextId ()
            {
                _id++;
                return _id;
            }
        }
    }
}