﻿using UnityEngine;

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{

    public class Mat : DisposableOpenCVObject
    {


        protected override void Dispose (bool disposing)
        {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER

            try {
                                
                if (disposing) {
                }
                                
                if (IsEnabledDispose) {
                    if (nativeObj != IntPtr.Zero)
                        core_Mat_n_1delete (nativeObj);
                    nativeObj = IntPtr.Zero;
                }

            } finally {
                base.Dispose (disposing);
            }

#else

#endif
        }

        public Mat (IntPtr addr)
        {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER

            if (addr == IntPtr.Zero)
                throw new CvException ("Native object address is NULL");
            nativeObj = addr;
#else

#endif
        }
			
        public Mat ()
        {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            nativeObj = core_Mat_n_1Mat__ ();
        
            return;
#else

#endif
        }

        public Mat (int rows, int cols, int type)
        {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            nativeObj = core_Mat_n_1Mat__III (rows, cols, type);
        
            return;
#else

#endif
        }

        public Mat (Size size, int type)
        {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            nativeObj = core_Mat_n_1Mat__DDI (size.width, size.height, type);
        
            return;
#else

#endif
        }


        public Mat (int rows, int cols, int type, Scalar s)
        {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            nativeObj = core_Mat_n_1Mat__IIIDDDD (rows, cols, type, s.val [0], s.val [1], s.val [2], s.val [3]);

        
            return;
#else

#endif
        }



        public Mat (Size size, int type, Scalar s)
        {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            nativeObj = core_Mat_n_1Mat__DDIDDDD (size.width, size.height, type, s.val [0], s.val [1], s.val [2], s.val [3]);
        
            return;
#else

#endif
        }

        public Mat (Mat m, Range rowRange, Range colRange)
        {
            if (m != null)
                m.ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            nativeObj = core_Mat_n_1Mat__JIIII (m.nativeObj, rowRange.start, rowRange.end, colRange.start, colRange.end);
        
            return;
#else

#endif
        }


        public Mat (Mat m, Range rowRange)
        {
            if (m != null)
                m.ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            nativeObj = core_Mat_n_1Mat__JII (m.nativeObj, rowRange.start, rowRange.end);
        
            return;
#else

#endif
        }


        public Mat (Mat m, Rect roi)
        {
            if (m != null)
                m.ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER

        
            nativeObj = core_Mat_n_1Mat__JIIII (m.nativeObj, roi.y, roi.y + roi.height, roi.x, roi.x + roi.width);
        
            return;
#else

#endif
        }
			
        public Mat adjustROI (int dtop, int dbottom, int dleft, int dright)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            Mat retVal = new Mat (core_Mat_n_1adjustROI (nativeObj, dtop, dbottom, dleft, dright));
        
            return retVal;
#else
            return null;
#endif
        }


        public void assignTo (Mat m, int type)
        {
            if (m != null)
                m.ThrowIfDisposed ();
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            core_Mat_n_1assignTo__JJI (nativeObj, m.nativeObj, type);
        
            return;
#else

#endif
        }
			
        public void assignTo (Mat m)
        {
            if (m != null)
                m.ThrowIfDisposed ();
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            core_Mat_n_1assignTo__JJ (nativeObj, m.nativeObj);
        
            return;
#else

#endif
        }
			
        public int channels ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            int retVal = core_Mat_n_1channels (nativeObj);
        
            return retVal;
#else
            return 0;
#endif
        }

        public int checkVector (int elemChannels, int depth, bool requireContinuous)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            int retVal = core_Mat_n_1checkVector__JIIZ (nativeObj, elemChannels, depth, requireContinuous);
        
            return retVal;
#else
            return 0;
#endif
        }

        public int checkVector (int elemChannels, int depth)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            int retVal = core_Mat_n_1checkVector__JII (nativeObj, elemChannels, depth);
        
            return retVal;
#else
            return 0;
#endif
        }

        public int checkVector (int elemChannels)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            int retVal = core_Mat_n_1checkVector__JI (nativeObj, elemChannels);
        
            return retVal;
#else
            return 0;
#endif
        }

        public Mat clone ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            Mat retVal = new Mat (core_Mat_n_1clone (nativeObj));
        
            return retVal;
#else
            return null;
#endif
        }
			
        public Mat col (int x)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            Mat retVal = new Mat (core_Mat_n_1col (nativeObj, x));
        
            return retVal;
#else
            return null;
#endif
        }

        public Mat colRange (int startcol, int endcol)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            Mat retVal = new Mat (core_Mat_n_1colRange (nativeObj, startcol, endcol));
        
            return retVal;
#else
            return null;
#endif
        }

        public Mat colRange (Range r)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            Mat retVal = new Mat (core_Mat_n_1colRange (nativeObj, r.start, r.end));
        
            return retVal;
#else
            return null;
#endif
        }

        public int dims ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            int retVal = core_Mat_n_1dims (nativeObj);
        
            return retVal;
#else
            return 0;
#endif
        }
			
        public int cols ()
        {
            ThrowIfDisposed ();


#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            int retVal = core_Mat_n_1cols (nativeObj);
        
            return retVal;
#else
            return 0;
#endif
        }

        public void convertTo (Mat m, int rtype, double alpha, double beta)
        {
            if (m != null)
                m.ThrowIfDisposed ();
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            core_Mat_n_1convertTo__JJIDD (nativeObj, m.nativeObj, rtype, alpha, beta);
        
            return;
#else
            return;
#endif
        }

        public void convertTo (Mat m, int rtype, double alpha)
        {
            if (m != null)
                m.ThrowIfDisposed ();
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            core_Mat_n_1convertTo__JJID (nativeObj, m.nativeObj, rtype, alpha);
        
            return;
#else
            return;
#endif
        }

        public void convertTo (Mat m, int rtype)
        {
            if (m != null)
                m.ThrowIfDisposed ();
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            core_Mat_n_1convertTo__JJI (nativeObj, m.nativeObj, rtype);
        
            return;
#else
            return;
#endif
        }
			
        public void copyTo (Mat m)
        {
            if (m != null)
                m.ThrowIfDisposed ();
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            core_Mat_n_1copyTo__JJ (nativeObj, m.nativeObj);
        
            return;
#else
            return;
#endif
        }
			
        public void copyTo (Mat m, Mat mask)
        {
            if (m != null)
                m.ThrowIfDisposed ();
            if (mask != null)
                mask.ThrowIfDisposed ();
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
            core_Mat_n_1copyTo__JJJ (nativeObj, m.nativeObj, mask.nativeObj);
        
            return;
#else
            return;
#endif
        }


        public void create (int rows, int cols, int type)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            core_Mat_n_1create__JIII (nativeObj, rows, cols, type);
        
            return;
#else
            return;
#endif
        }
			
        public void create (Size size, int type)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            core_Mat_n_1create__JDDI (nativeObj, size.width, size.height, type);
        
            return;
#else
            return;
#endif
        }
			
        public Mat cross (Mat m)
        {
            if (m != null)
                m.ThrowIfDisposed ();
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1cross (nativeObj, m.nativeObj));
        
            return retVal;
#else
            return null;
#endif
        }

        //
        // C++: long Mat::dataAddr()
        //

        public long dataAddr ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            long retVal = core_Mat_n_1dataAddr (nativeObj);
        
            return retVal;
#else
            return 0;
#endif
        }
			
        public int depth ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            int retVal = core_Mat_n_1depth (nativeObj);
        
            return retVal;
#else
            return 0;
#endif
        }

        public Mat diag (int d)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1diag__JI (nativeObj, d));
        
            return retVal;
#else
            return null;
#endif
        }

        public Mat diag ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1diag__JI (nativeObj, 0));
        
            return retVal;
#else
            return null;
#endif
        }


        public static Mat diag (Mat d)
        {
            if (d != null)
                d.ThrowIfDisposed ();


#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1diag__J (d.nativeObj));
        
            return retVal;
#else
            return null;
#endif
        }
			
        public double dot (Mat m)
        {
            if (m != null)
                m.ThrowIfDisposed ();
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            double retVal = core_Mat_n_1dot (nativeObj, m.nativeObj);
        
            return retVal;
#else
            return 0;
#endif
        }

        public long elemSize ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            long retVal = core_Mat_n_1elemSize (nativeObj);//TODO: @size_t long long
        
            return retVal;
#else
            return 0;
#endif
        }

        public long elemSize1 ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            long retVal = core_Mat_n_1elemSize1 (nativeObj);//TODO: @size_t long long
        
            return retVal;
#else
            return 0;
#endif
        }
			
        public bool empty ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            bool retVal = core_Mat_n_1empty (nativeObj);
        
            return retVal;
#else
            return false;
#endif
        }
			
        public static Mat eye (int rows, int cols, int type)
        {

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1eye__III (rows, cols, type));
        
            return retVal;
#else
            return null;
#endif
        }
			
        public static Mat eye (Size size, int type)
        {

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1eye__DDI (size.width, size.height, type));
        
            return retVal;
#else
            return null;
#endif
        }
			
        public Mat inv (int method)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1inv__JI (nativeObj, method));
        
            return retVal;
#else
            return null;
#endif
        }
			
        public Mat inv ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1inv__J (nativeObj));
        
            return retVal;
#else
            return null;
#endif
        }
			
        public bool isContinuous ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            bool retVal = core_Mat_n_1isContinuous (nativeObj);
        
            return retVal;
#else
            return false;
#endif
        }

        //
        // C++: bool Mat::isSubmatrix()
        //

        public bool isSubmatrix ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            bool retVal = core_Mat_n_1isSubmatrix (nativeObj);
        
            return retVal;
#else
            return false;
#endif
        }
			
        public void locateROI (Size wholeSize, Point ofs)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER

            double[] wholeSize_out = new double[2];
            double[] ofs_out = new double[2];
            core_Mat_locateROI_10 (nativeObj, wholeSize_out, ofs_out);
            if (wholeSize != null) {
                wholeSize.width = wholeSize_out [0];
                wholeSize.height = wholeSize_out [1];
            }
            if (ofs != null) {
                ofs.x = ofs_out [0];
                ofs.y = ofs_out [1];
            }
            return;
#else
            return;
#endif
        }
			
        public Mat mul (Mat m, double scale)
        {
            if (m != null)
                m.ThrowIfDisposed ();
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1mul__JJD (nativeObj, m.nativeObj, scale));
        
            return retVal;
#else
            return null;
#endif
        }
			
        public Mat mul (Mat m)
        {
            if (m != null)
                m.ThrowIfDisposed ();
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1mul__JJ (nativeObj, m.nativeObj));
        
            return retVal;
#else
            return null;
#endif
        }

        public static Mat ones (int rows, int cols, int type)
        {

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1ones__III (rows, cols, type));
        
            return retVal;
#else
            return null;
#endif
        }


        public static Mat ones (Size size, int type)
        {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1ones__DDI (size.width, size.height, type));
        
            return retVal;
#else
            return null;
#endif
        }


        public void push_back (Mat m)
        {
            if (m != null)
                m.ThrowIfDisposed ();
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            core_Mat_n_1push_1back (nativeObj, m.nativeObj);
        
            return;
#else
            return;
#endif
        }
			
        public void release ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            core_Mat_n_1release (nativeObj);
        
            return;
#else
            return;
#endif
        }
			
        public Mat reshape (int cn, int rows)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1reshape__JII (nativeObj, cn, rows));
        
            return retVal;
#else
            return null;
#endif
        }


        public Mat reshape (int cn)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1reshape__JI (nativeObj, cn));
        
            return retVal;
#else
            return null;
#endif
        }


        public Mat row (int y)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1row (nativeObj, y));
        
            return retVal;
#else
            return null;
#endif
        }


        public Mat rowRange (int startrow, int endrow)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1rowRange (nativeObj, startrow, endrow));
        
            return retVal;
#else
            return null;
#endif
        }
			
        public Mat rowRange (Range r)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1rowRange (nativeObj, r.start, r.end));
        
            return retVal;
#else
            return null;
#endif
        }

        //
        // C++: int Mat::rows()
        //

        public int rows ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            int retVal = core_Mat_n_1rows (nativeObj);
        
            return retVal;
#else
            return 0;
#endif
        }

        //
        // C++: Mat Mat::operator =(Scalar s)
        //

        public Mat setTo (Scalar s)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1setTo__JDDDD (nativeObj, s.val [0], s.val [1], s.val [2], s.val [3]));
        
            return retVal;
#else
            return null;
#endif
        }


        public Mat setTo (Scalar value, Mat mask)
        {
            if (mask != null)
                mask.ThrowIfDisposed ();
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1setTo__JDDDDJ (nativeObj, value.val [0], value.val [1], value.val [2], value.val [3], mask.nativeObj));
        
            return retVal;
#else
            return null;
#endif
        }


        public Mat setTo (Mat value, Mat mask)
        {
            if (value != null)
                value.ThrowIfDisposed ();
            if (mask != null)
                mask.ThrowIfDisposed ();
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1setTo__JJJ (nativeObj, value.nativeObj, mask.nativeObj));
        
            return retVal;
#else
            return null;
#endif
        }


        public Mat setTo (Mat value)
        {
            if (value != null)
                value.ThrowIfDisposed ();
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1setTo__JJ (nativeObj, value.nativeObj));
        
            return retVal;
#else
            return null;
#endif
        }


        public Size size ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
//          Size retVal = new Size(opencv_core_Mat_n_1size(nativeObj));
//      
//      return retVal;

            double[] tmpArray = new double[2];
            core_Mat_n_1size (nativeObj, tmpArray);

            Size retVal = new Size (tmpArray);

            return retVal;
#else
            return null;
#endif
        }
			
        public long step1 (int i)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            long retVal = core_Mat_n_1step1__JI (nativeObj, i);
        
            return retVal;
#else
            return 0;
#endif
        }
			
        public long step1 ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            long retVal = core_Mat_n_1step1__J (nativeObj);
        
            return retVal;
#else
            return 0;
#endif
        }
			
        public Mat submat (int rowStart, int rowEnd, int colStart, int colEnd)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1submat_1rr (nativeObj, rowStart, rowEnd, colStart, colEnd));
        
            return retVal;
#else
            return null;
#endif
        }
			
        public Mat submat (Range rowRange, Range colRange)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1submat_1rr (nativeObj, rowRange.start, rowRange.end, colRange.start, colRange.end));
        
            return retVal;
#else
            return null;
#endif
        }
			
        public Mat submat (Rect roi)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1submat (nativeObj, roi.x, roi.y, roi.width, roi.height));
        
            return retVal;
#else
            return null;
#endif
        }
			
        public Mat t ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1t (nativeObj));
        
            return retVal;
#else
            return null;
#endif
        }
			
        public long total ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            long retVal = core_Mat_n_1total (nativeObj);
        
            return retVal;
#else
            return 0;
#endif
        }

        public int type ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            int retVal = core_Mat_n_1type (nativeObj);
        
            return retVal;
#else
            return 0;
#endif
        }


        public static Mat zeros (int rows, int cols, int type)
        {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1zeros__III (rows, cols, type));
        
            return retVal;
#else
            return null;
#endif
        }


        public static Mat zeros (Size size, int type)
        {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            Mat retVal = new Mat (core_Mat_n_1zeros__DDI (size.width, size.height, type));
        
            return retVal;
#else
            return null;
#endif
        }

        //@Override
        public override string ToString ()
        {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            return "Mat [ " +
            rows () + "*" + cols () + "*" + CvType.typeToString (type ()) +
            ", isCont=" + isContinuous () + ", isSubmat=" + isSubmatrix () +
            ", nativeObj=0x" + Convert.ToString (nativeObj) +
            ", dataAddr=0x" + Convert.ToString (dataAddr ()) +
            " ]";
#else
            return null;
#endif

        }

        public string dump ()
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            return Marshal.PtrToStringAnsi (core_Mat_nDump (nativeObj));
#else
            return null;
#endif
        }

        public int put (int row, int col, params double[] data)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            int t = type ();
            if (data == null || data.Length % CvType.channels (t) != 0)
                throw new CvException (
                    "Provided data element number (" +
                    (data == null ? 0 : data.Length) +
                    ") should be multiple of the Mat channels count (" +
                    CvType.channels (t) + ")");
            return core_Mat_nPutD (nativeObj, row, col, data.Length, data);
#else
            return 0;
#endif
        }

        public int put (int row, int col, float[] data)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            int t = type ();
            if (data == null || data.Length % CvType.channels (t) != 0)
                throw new CvException (
                    "Provided data element number (" +
                    (data == null ? 0 : data.Length) +
                    ") should be multiple of the Mat channels count (" +
                    CvType.channels (t) + ")");
            if (CvType.depth (t) == CvType.CV_32F) {
                return core_Mat_nPutF (nativeObj, row, col, data.Length, data);
            }
            throw new CvException ("Mat data type is not compatible: " + t);
#else
            return 0;
#endif
        }

        public int put (int row, int col, int[] data)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            int t = type ();
            if (data == null || data.Length % CvType.channels (t) != 0)
                throw new CvException (
                    "Provided data element number (" +
                    (data == null ? 0 : data.Length) +
                    ") should be multiple of the Mat channels count (" +
                    CvType.channels (t) + ")");
            if (CvType.depth (t) == CvType.CV_32S) {
                return core_Mat_nPutI (nativeObj, row, col, data.Length, data);
            }
            throw new CvException ("Mat data type is not compatible: " + t);
#else
            return 0;
#endif
        }

        public int put (int row, int col, short[] data)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            int t = type ();
            if (data == null || data.Length % CvType.channels (t) != 0)
                throw new CvException (
                    "Provided data element number (" +
                    (data == null ? 0 : data.Length) +
                    ") should be multiple of the Mat channels count (" +
                    CvType.channels (t) + ")");
            if (CvType.depth (t) == CvType.CV_16U || CvType.depth (t) == CvType.CV_16S) {
                return core_Mat_nPutS (nativeObj, row, col, data.Length, data);
            }
            throw new CvException ("Mat data type is not compatible: " + t);
#else
            return 0;
#endif
        }

        public int put (int row, int col, byte[] data)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            int t = type ();
            if (data == null || data.Length % CvType.channels (t) != 0)
                throw new CvException (
                    "Provided data element number (" +
                    (data == null ? 0 : data.Length) +
                    ") should be multiple of the Mat channels count (" +
                    CvType.channels (t) + ")");
            if (CvType.depth (t) == CvType.CV_8U || CvType.depth (t) == CvType.CV_8S) {
                return core_Mat_nPutB (nativeObj, row, col, data.Length, data);
            }
            throw new CvException ("Mat data type is not compatible: " + t);
#else
            return 0;
#endif
        }

        public int get (int row, int col, byte[] data)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            int t = type ();
            if (data == null || data.Length % CvType.channels (t) != 0)
                throw new CvException (
                    "Provided data element number (" +
                    (data == null ? 0 : data.Length) +
                    ") should be multiple of the Mat channels count (" +
                    CvType.channels (t) + ")");
            if (CvType.depth (t) == CvType.CV_8U || CvType.depth (t) == CvType.CV_8S) {
                return core_Mat_nGetB (nativeObj, row, col, data.Length, data);
            }
            throw new CvException ("Mat data type is not compatible: " + t);
#else
            return 0;
#endif
        }

        public int get (int row, int col, short[] data)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            int t = type ();
            if (data == null || data.Length % CvType.channels (t) != 0)
                throw new CvException (
                    "Provided data element number (" +
                    (data == null ? 0 : data.Length) +
                    ") should be multiple of the Mat channels count (" +
                    CvType.channels (t) + ")");
            if (CvType.depth (t) == CvType.CV_16U || CvType.depth (t) == CvType.CV_16S) {
                return core_Mat_nGetS (nativeObj, row, col, data.Length, data);
            }
            throw new CvException ("Mat data type is not compatible: " + t);
#else
            return 0;
#endif
        }

        public int get (int row, int col, int[] data)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            int t = type ();
            if (data == null || data.Length % CvType.channels (t) != 0)
                throw new CvException (
                    "Provided data element number (" +
                    (data == null ? 0 : data.Length) +
                    ") should be multiple of the Mat channels count (" +
                    CvType.channels (t) + ")");
            if (CvType.depth (t) == CvType.CV_32S) {
                return core_Mat_nGetI (nativeObj, row, col, data.Length, data);
            }
            throw new CvException ("Mat data type is not compatible: " + t);
#else
            return 0;
#endif
        }

        public int get (int row, int col, float[] data)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            int t = type ();
            if (data == null || data.Length % CvType.channels (t) != 0)
                throw new CvException (
                    "Provided data element number (" +
                    (data == null ? 0 : data.Length) +
                    ") should be multiple of the Mat channels count (" +
                    CvType.channels (t) + ")");
            if (CvType.depth (t) == CvType.CV_32F) {
                return core_Mat_nGetF (nativeObj, row, col, data.Length, data);
            }
            throw new CvException ("Mat data type is not compatible: " + t);
#else
            return 0;
#endif
        }

        public int get (int row, int col, double[] data)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            int t = type ();
            if (data == null || data.Length % CvType.channels (t) != 0)
                throw new CvException (
                    "Provided data element number (" +
                    (data == null ? 0 : data.Length) +
                    ") should be multiple of the Mat channels count (" +
                    CvType.channels (t) + ")");
            if (CvType.depth (t) == CvType.CV_64F) {
                return core_Mat_nGetD (nativeObj, row, col, data.Length, data);
            }
            throw new CvException ("Mat data type is not compatible: " + t);
#else
            return 0;
#endif
        }

        public double[] get (int row, int col)
        {
            ThrowIfDisposed ();

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER

            double[] tmpArray = new double[channels ()];
            int result = core_Mat_nGet (nativeObj, row, col, tmpArray.Length, tmpArray);

            if (result == 0) {
                return null;
            } else {
                return tmpArray;
            }
#else
            return null;
#endif
        }

        public int height ()
        {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            return rows ();
#else
            return 0;
#endif
        }

        public int width ()
        {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
            return cols ();
#else
            return 0;
#endif
        }

        public IntPtr getNativeObjAddr ()
        {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
                    

            return nativeObj;
#else
            return IntPtr.Zero;
#endif
        }


        #if (UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR
        const string LIBNAME = "__Internal";

#else
        const string LIBNAME = "opencvforunity";
        #endif

        // C++: Mat::Mat()
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1Mat__ ();

        // C++: Mat::Mat(int rows, int cols, int type)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1Mat__III (int rows, int cols, int type);

        // C++: Mat::Mat(Size size, int type)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1Mat__DDI (double size_width, double size_height, int type);

        // C++: Mat::Mat(int rows, int cols, int type, Scalar s)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1Mat__IIIDDDD (int rows, int cols, int type, double s_val0, double s_val1, double s_val2, double s_val3);

        // C++: Mat::Mat(Size size, int type, Scalar s)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1Mat__DDIDDDD (double size_width, double size_height, int type, double s_val0, double s_val1, double s_val2, double s_val3);

        // C++: Mat::Mat(Mat m, Range rowRange, Range colRange = Range::all())
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1Mat__JIIII (IntPtr m_nativeObj, int rowRange_start, int rowRange_end, int colRange_start, int colRange_end);

        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1Mat__JII (IntPtr m_nativeObj, int rowRange_start, int rowRange_end);

        // C++: Mat Mat::adjustROI(int dtop, int dbottom, int dleft, int dright)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1adjustROI (IntPtr nativeObj, int dtop, int dbottom, int dleft, int dright);

        // C++: void Mat::assignTo(Mat m, int type = -1)
        [DllImport (LIBNAME)]
        private static extern void core_Mat_n_1assignTo__JJI (IntPtr nativeObj, IntPtr m_nativeObj, int type);

        [DllImport (LIBNAME)]
        private static extern void core_Mat_n_1assignTo__JJ (IntPtr nativeObj, IntPtr m_nativeObj);

        // C++: int Mat::channels()
        [DllImport (LIBNAME)]
        private static extern int core_Mat_n_1channels (IntPtr nativeObj);

        // C++: int Mat::checkVector(int elemChannels, int depth = -1, bool
        // requireContinuous = true)
        [DllImport (LIBNAME)]
        private static extern int core_Mat_n_1checkVector__JIIZ (IntPtr nativeObj, int elemChannels, int depth, bool requireContinuous);

        [DllImport (LIBNAME)]
        private static extern int core_Mat_n_1checkVector__JII (IntPtr nativeObj, int elemChannels, int depth);

        [DllImport (LIBNAME)]
        private static extern int core_Mat_n_1checkVector__JI (IntPtr nativeObj, int elemChannels);

        // C++: Mat Mat::clone()
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1clone (IntPtr nativeObj);

        // C++: Mat Mat::col(int x)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1col (IntPtr nativeObj, int x);

        // C++: Mat Mat::colRange(int startcol, int endcol)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1colRange (IntPtr nativeObj, int startcol, int endcol);

        // C++: int Mat::dims()
        [DllImport (LIBNAME)]
        private static extern int core_Mat_n_1dims (IntPtr nativeObj);

        // C++: int Mat::cols()
        [DllImport (LIBNAME)]
        private static extern int core_Mat_n_1cols (IntPtr nativeObj);

        // C++: void Mat::convertTo(Mat& m, int rtype, double alpha = 1, double beta
        // = 0)
        [DllImport (LIBNAME)]
        private static extern void core_Mat_n_1convertTo__JJIDD (IntPtr nativeObj, IntPtr m_nativeObj, int rtype, double alpha, double beta);

        [DllImport (LIBNAME)]
        private static extern void core_Mat_n_1convertTo__JJID (IntPtr nativeObj, IntPtr m_nativeObj, int rtype, double alpha);

        [DllImport (LIBNAME)]
        private static extern void core_Mat_n_1convertTo__JJI (IntPtr nativeObj, IntPtr m_nativeObj, int rtype);

        // C++: void Mat::copyTo(Mat& m)
        [DllImport (LIBNAME)]
        private static extern void core_Mat_n_1copyTo__JJ (IntPtr nativeObj, IntPtr m_nativeObj);

        // C++: void Mat::copyTo(Mat& m, Mat mask)
        [DllImport (LIBNAME)]
        private static extern void core_Mat_n_1copyTo__JJJ (IntPtr nativeObj, IntPtr m_nativeObj, IntPtr mask_nativeObj);

        // C++: void Mat::create(int rows, int cols, int type)
        [DllImport (LIBNAME)]
        private static extern void core_Mat_n_1create__JIII (IntPtr nativeObj, int rows, int cols, int type);

        // C++: void Mat::create(Size size, int type)
        [DllImport (LIBNAME)]
        private static extern void core_Mat_n_1create__JDDI (IntPtr nativeObj, double size_width, double size_height, int type);

        // C++: Mat Mat::cross(Mat m)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1cross (IntPtr nativeObj, IntPtr m_nativeObj);

        // C++: long Mat::dataAddr()
        [DllImport (LIBNAME)]
        private static extern long core_Mat_n_1dataAddr (IntPtr nativeObj);

        // C++: int Mat::depth()
        [DllImport (LIBNAME)]
        private static extern int core_Mat_n_1depth (IntPtr nativeObj);

        // C++: Mat Mat::diag(int d = 0)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1diag__JI (IntPtr nativeObj, int d);

        // C++: static Mat Mat::diag(Mat d)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1diag__J (IntPtr d_nativeObj);

        // C++: double Mat::dot(Mat m)
        [DllImport (LIBNAME)]
        private static extern double core_Mat_n_1dot (IntPtr nativeObj, IntPtr m_nativeObj);

        // C++: size_t Mat::elemSize()
        [DllImport (LIBNAME)]
        private static extern long core_Mat_n_1elemSize (IntPtr nativeObj);

        // C++: size_t Mat::elemSize1()
        [DllImport (LIBNAME)]
        private static extern long core_Mat_n_1elemSize1 (IntPtr nativeObj);

        // C++: bool Mat::empty()
        [DllImport (LIBNAME)]
        private static extern bool core_Mat_n_1empty (IntPtr nativeObj);

        // C++: static Mat Mat::eye(int rows, int cols, int type)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1eye__III (int rows, int cols, int type);

        // C++: static Mat Mat::eye(Size size, int type)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1eye__DDI (double size_width, double size_height, int type);

        // C++: Mat Mat::inv(int method = DECOMP_LU)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1inv__JI (IntPtr nativeObj, int method);

        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1inv__J (IntPtr nativeObj);

        // C++: bool Mat::isContinuous()
        [DllImport (LIBNAME)]
        private static extern bool core_Mat_n_1isContinuous (IntPtr nativeObj);

        // C++: bool Mat::isSubmatrix()
        [DllImport (LIBNAME)]
        private static extern bool core_Mat_n_1isSubmatrix (IntPtr nativeObj);

        // C++: void Mat::locateROI(Size wholeSize, Point ofs)
        [DllImport (LIBNAME)]
        private static extern void core_Mat_locateROI_10 (IntPtr nativeObj, [In, Out, MarshalAs (UnmanagedType.LPArray, SizeConst = 2)] double[] wholeSize_out, [In, Out, MarshalAs (UnmanagedType.LPArray, SizeConst = 2)] double[] ofs_out);

        // C++: Mat Mat::mul(Mat m, double scale = 1)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1mul__JJD (IntPtr nativeObj, IntPtr m_nativeObj, double scale);

        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1mul__JJ (IntPtr nativeObj, IntPtr m_nativeObj);

        // C++: static Mat Mat::ones(int rows, int cols, int type)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1ones__III (int rows, int cols, int type);

        // C++: static Mat Mat::ones(Size size, int type)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1ones__DDI (double size_width, double size_height, int type);

        // C++: void Mat::push_back(Mat m)
        [DllImport (LIBNAME)]
        private static extern void core_Mat_n_1push_1back (IntPtr nativeObj, IntPtr m_nativeObj);

        // C++: void Mat::release()
        [DllImport (LIBNAME)]
        private static extern void core_Mat_n_1release (IntPtr nativeObj);

        // C++: Mat Mat::reshape(int cn, int rows = 0)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1reshape__JII (IntPtr nativeObj, int cn, int rows);

        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1reshape__JI (IntPtr nativeObj, int cn);

        // C++: Mat Mat::row(int y)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1row (IntPtr nativeObj, int y);

        // C++: Mat Mat::rowRange(int startrow, int endrow)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1rowRange (IntPtr nativeObj, int startrow, int endrow);

        // C++: int Mat::rows()
        [DllImport (LIBNAME)]
        private static extern int core_Mat_n_1rows (IntPtr nativeObj);

        // C++: Mat Mat::operator =(Scalar s)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1setTo__JDDDD (IntPtr nativeObj, double s_val0, double s_val1, double s_val2, double s_val3);

        // C++: Mat Mat::setTo(Scalar value, Mat mask = Mat())
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1setTo__JDDDDJ (IntPtr nativeObj, double s_val0, double s_val1, double s_val2, double s_val3, IntPtr mask_nativeObj);

        // C++: Mat Mat::setTo(Mat value, Mat mask = Mat())
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1setTo__JJJ (IntPtr nativeObj, IntPtr value_nativeObj, IntPtr mask_nativeObj);

        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1setTo__JJ (IntPtr nativeObj, IntPtr value_nativeObj);

        // C++: Size Mat::size()
        [DllImport (LIBNAME)]
        private static extern void core_Mat_n_1size (IntPtr nativeObj,
                                                     [In, Out, MarshalAs (UnmanagedType.LPArray, SizeConst = 2)] double[] vals);


        // C++: size_t Mat::step1(int i = 0)
        [DllImport (LIBNAME)]
        private static extern long core_Mat_n_1step1__JI (IntPtr nativeObj, int i);

        [DllImport (LIBNAME)]
        private static extern long core_Mat_n_1step1__J (IntPtr nativeObj);

        // C++: Mat Mat::operator()(Range rowRange, Range colRange)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1submat_1rr (IntPtr nativeObj, int rowRange_start, int rowRange_end, int colRange_start, int colRange_end);

        // C++: Mat Mat::operator()(Rect roi)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1submat (IntPtr nativeObj, int roi_x, int roi_y, int roi_width, int roi_height);

        // C++: Mat Mat::t()
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1t (IntPtr nativeObj);

        // C++: size_t Mat::total()
        [DllImport (LIBNAME)]
        private static extern long core_Mat_n_1total (IntPtr nativeObj);

        // C++: int Mat::type()
        [DllImport (LIBNAME)]
        private static extern int core_Mat_n_1type (IntPtr nativeObj);

        // C++: static Mat Mat::zeros(int rows, int cols, int type)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1zeros__III (int rows, int cols, int type);

        // C++: static Mat Mat::zeros(Size size, int type)
        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_n_1zeros__DDI (double size_width, double size_height, int type);

        // native support for java finalize()
        [DllImport (LIBNAME)]
        private static extern void core_Mat_n_1delete (IntPtr nativeObj);

        [DllImport (LIBNAME)]
        private static extern int core_Mat_nPutD (IntPtr self, int row, int col, int count, [In, MarshalAs (UnmanagedType.LPArray, SizeParamIndex = 3)] double[] data);

        [DllImport (LIBNAME)]
        private static extern int core_Mat_nPutF (IntPtr self, int row, int col, int count, [In, MarshalAs (UnmanagedType.LPArray, SizeParamIndex = 3)] float[] data);

        [DllImport (LIBNAME)]
        private static extern int core_Mat_nPutI (IntPtr self, int row, int col, int count, [In, MarshalAs (UnmanagedType.LPArray, SizeParamIndex = 3)] int[] data);

        [DllImport (LIBNAME)]
        private static extern int core_Mat_nPutS (IntPtr self, int row, int col, int count, [In, MarshalAs (UnmanagedType.LPArray, SizeParamIndex = 3)] short[] data);

        [DllImport (LIBNAME)]
        private static extern int core_Mat_nPutB (IntPtr self, int row, int col, int count, [In, MarshalAs (UnmanagedType.LPArray, SizeParamIndex = 3)] byte[] data);

        [DllImport (LIBNAME)]
        private static extern int core_Mat_nGetB (IntPtr self, int row, int col, int count, [In, Out, MarshalAs (UnmanagedType.LPArray, SizeParamIndex = 3)] byte[] vals);

        [DllImport (LIBNAME)]
        private static extern int core_Mat_nGetS (IntPtr self, int row, int col, int count, [In, Out, MarshalAs (UnmanagedType.LPArray, SizeParamIndex = 3)] short[] vals);

        [DllImport (LIBNAME)]
        private static extern int core_Mat_nGetI (IntPtr self, int row, int col, int count, [In, Out, MarshalAs (UnmanagedType.LPArray, SizeParamIndex = 3)] int[] vals);

        [DllImport (LIBNAME)]
        private static extern int core_Mat_nGetF (IntPtr self, int row, int col, int count, [In, Out, MarshalAs (UnmanagedType.LPArray, SizeParamIndex = 3)] float[] vals);

        [DllImport (LIBNAME)]
        private static extern int core_Mat_nGetD (IntPtr self, int row, int col, int count, [In, Out, MarshalAs (UnmanagedType.LPArray, SizeParamIndex = 3)] double[] vals);

        [DllImport (LIBNAME)]
        private static extern int core_Mat_nGet (IntPtr self, int row, int col, int count, [In, Out, MarshalAs (UnmanagedType.LPArray, SizeParamIndex = 3)] double[] vals);

        [DllImport (LIBNAME)]
        private static extern IntPtr core_Mat_nDump (IntPtr self);

        //

        #region Operators

        #if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5 || UNITY_5_3_OR_NEWER
        
        #region Unary

        #region +

        public static Mat operator + (Mat mat)
        {
            return mat;
        }

        #endregion

        #region -

        public static Mat operator - (Mat mat)
        {
            Mat m = new Mat ();
            Core.multiply (mat, new Scalar (-1, -1, -1, -1), m);
            return m;
        }

        #endregion

        #endregion

        #region Binary

        #region +

        public static Mat operator + (Mat a, Mat b)
        {
            Mat m = new Mat ();
            Core.add (a, b, m);
            return m;
        }

        public static Mat operator + (Mat a, Scalar s)
        {
            Mat m = new Mat ();
            Core.add (a, s, m);
            return m;
        }

        public static Mat operator + (Scalar s, Mat a)
        {
            Mat m = new Mat ();
            Core.add (a, s, m);
            return m;
        }

        #endregion

        #region -

        public static Mat operator - (Mat a, Mat b)
        {
            Mat m = new Mat ();
            Core.subtract (a, b, m);
            return m;
        }

        public static Mat operator - (Mat a, Scalar s)
        {
            Mat m = new Mat ();
            Core.subtract (a, s, m);
            return m;
        }

        public static Mat operator - (Scalar s, Mat a)
        {
            Mat m = new Mat ();
            using (Mat b = new Mat (a.size (), a.type (), s)) {
                Core.subtract (b, a, m);
            }
            return m;
        }

        #endregion

        #region *

        public static Mat operator * (Mat a, Mat b)
        {
            Mat m = new Mat ();
            Core.gemm (a, b, 1, new Mat (), 0, m);
            return m;
        }

        public static Mat operator * (Mat a, double s)
        {
            Mat m = new Mat ();
            Core.multiply (a, Scalar.all (s), m);
            return m;
        }

        public static Mat operator * (double s, Mat a)
        {
            Mat m = new Mat ();
            Core.multiply (a, Scalar.all (s), m);
            return m;
        }

        #endregion

        #region /

        public static Mat operator / (Mat a, Mat b)
        {
            Mat m = new Mat ();
            Core.divide (a, b, m);
            return m;
        }

        public static Mat operator / (Mat a, double s)
        {
            Mat m = new Mat ();
            Core.divide (a, Scalar.all (s), m);
            return m;
        }

        public static Mat operator / (double s, Mat a)
        {
            Mat m = new Mat ();
            using (Mat b = new Mat (a.size (), a.type (), Scalar.all (s))) {
                Core.divide (b, a, m);
            }
            return m;
        }

        #endregion

        #region &

        public static Mat operator & (Mat a, Mat b)
        {
            Mat m = new Mat ();
            Core.bitwise_and (a, b, m);
            return m;
        }

        public static Mat operator & (Mat a, double s)
        {
            Mat m = new Mat ();
            using (Mat b = new Mat (a.size (), a.type (), new Scalar (s))) {
                Core.bitwise_and (a, b, m);
            }
            return m;
        }

        public static Mat operator & (double s, Mat a)
        {
            Mat m = new Mat ();
            using (Mat b = new Mat (a.size (), a.type (), new Scalar (s))) {
                Core.bitwise_and (b, a, m);
            }
            return m;
        }

        #endregion

        #region |

        public static Mat operator | (Mat a, Mat b)
        {
            Mat m = new Mat ();
            Core.bitwise_or (a, b, m);
            return m;
        }

        public static Mat operator | (Mat a, double s)
        {
            Mat m = new Mat ();
            using (Mat b = new Mat (a.size (), a.type (), new Scalar (s))) {
                Core.bitwise_or (a, b, m);
            }
            return m;
        }

        public static Mat operator | (double s, Mat a)
        {
            Mat m = new Mat ();
            using (Mat b = new Mat (a.size (), a.type (), new Scalar (s))) {
                Core.bitwise_or (b, a, m);
            }
            return m;
        }

        #endregion

        #region ^

        public static Mat operator ^ (Mat a, Mat b)
        {
            Mat m = new Mat ();
            Core.bitwise_xor (a, b, m);
            return m;
        }

        public static Mat operator ^ (Mat a, double s)
        {
            Mat m = new Mat ();
            using (Mat b = new Mat (a.size (), a.type (), new Scalar (s))) {
                Core.bitwise_xor (a, b, m);
            }
            return m;
        }

        public static Mat operator ^ (double s, Mat a)
        {
            Mat m = new Mat ();
            using (Mat b = new Mat (a.size (), a.type (), new Scalar (s))) {
                Core.bitwise_xor (b, a, m);
            }
            return m;
        }

        #endregion

        #region ~

        public static Mat operator ~ (Mat a)
        {
            Mat m = new Mat ();
            Core.bitwise_not (a, m);
            return m;
        }

        #endregion

        #endregion

        #endif

        #endregion

        //
    }
}
